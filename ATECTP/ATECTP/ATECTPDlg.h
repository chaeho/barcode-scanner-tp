
// ATECTPDlg.h : 헤더 파일
//

#pragma once

#include "ImageView.h"
#include "AtecScanLib.h"
#include "afxwin.h"


using namespace std;

#define NUM_OMR_LINE                    (32)
#define NUM_MARKER_PER_LINE             (16)

/* calibration step definition */
#define CALIB_STEP_NONE                 (0)
#define CALIB_STEP_OUT                  (1)
#define CALIB_STEP_IN                   (2)
#define CALIB_STEP_DOUBLE               (3)
#define CALIB_STEP_ABORT                (4)


// CATECTPDlg 대화 상자
class CATECTPDlg : public CDialogEx
{
// 생성입니다.
public:
	CATECTPDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ATECTP_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
    bool  m_bLedON_OFF;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()

public:
	BOOL		    m_bUSBOpen;
    unsigned char   *m_font;
    unsigned short  m_fontSize;
	CImageView      *m_pImageView_front;
	
	// Calibration step 변수
	unsigned char	m_step;

	afx_msg void OnBnClickedUsbOpen();
	afx_msg void OnBnClickedUsbClose();

	void OnPoolStart();
	void OnPoolStop();
	void OnMasData(CString msg);
	void BarCodeRead(unsigned char *rawfile, int TopOrBottom, int height);
	void InitWindows();
	int BigToWord(BYTE *pbSrcBig);
	int WordToBig(int wSrc, BYTE *pbDestBig);
	BYTE BinToBcd(BYTE Bin);
	void GetTimeBCD(BYTE* BCDTime);

	void Calib_Status(char status);
	void Brand_Status(char status);
	void Scan_Status(char status);
	void Device_Status(char status);
	void Check_Status(char *status);
    void SENSOR_Check(unsigned short status);

	// Calibration step 함수
	int Calibration_Step(unsigned char step);
	// Calibration 타임아웃 함수
	void OnCalibTimerStart();
	void OnCalibTimerStop();

    /* 버튼 핸들러 */
	afx_msg void OnBnClickedScanStart();
	afx_msg void OnBnClickedScanStop();
	afx_msg void OnBnClickedMotorStart();
	afx_msg void OnBnClickedMotorStop();
	afx_msg void OnBnClickedLedOnOff();
	afx_msg void OnBnClickedGetParameter();
	afx_msg void OnBnClickedSetParameter();	
	afx_msg void OnBnClickedSetSerial();
	afx_msg void OnBnClickedReset();	
	afx_msg void OnBnClickedCalibration();
	afx_msg void OnBnClickedFactoryInit();
	afx_msg void OnBnClickedFwUpdate();
    afx_msg void OnBnClickedFont();
    afx_msg void OnBnClickedFontDown();
};
