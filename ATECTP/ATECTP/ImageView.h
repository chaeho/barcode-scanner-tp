#pragma once


// CImageView

class CImageView : public CWnd
{
	DECLARE_DYNAMIC(CImageView)
	BYTE*	m_pData;
	int		m_nWidth;
	int		m_nHeight;
	int		m_nPxlFmt;

public:
	CImageView(CWnd* pParent);
	void	SetImage(BYTE* pData, int nWidth, int nHeight, int nPxlFmt);
	void	Make8bpp(BYTE* pData, int nWidth, int nHeight, BYTE* pImage, int offset, int stride);
	void	Make4bpp(BYTE* pData, int nWidth, int nHeight, BYTE* pImage, int offset, int stride);
	void	Make1bpp(BYTE* pData, int nWidth, int nHeight, BYTE* pImage, int offset, int stride);
	//{{AFX_VIRTUAL(CPreView)
public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	//}}AFX_VIRTUAL
public:
	virtual ~CImageView();

protected:
	//{{AFX_MSG(CImageView)	
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


