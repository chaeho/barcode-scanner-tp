//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// ATECTP.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ATECTP_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       129
#define IDC_USB_OPEN                    1000
#define IDC_USB_CLOSE                   1001
#define IDC_PACKET_STR                  1002
#define IDC_BARCODE_STR                 1003
#define IDC_SCAN_START                  1005
#define IDC_SCAN_STOP                   1006
#define IDC_MOTOR_START                 1007
#define IDC_MOTOR_STOP                  1008
#define IDC_MOTOR1                      1009
#define IDC_MOTOR2                      1010
#define IDC_CW                          1011
#define IDC_CCW                         1012
#define IDC_GET_PARAMETER               1015
#define IDC_SET_PARAMETER               1016
#define IDC_FONT                        1017
#define IDC_RADIO1                      1018
#define IDC_FONT_DOWN                   1019
#define IDC_LED_ON_OFF                  1020
#define IDC_INJECT                      1021
#define IDC_REJECT                      1022
#define IDC_SCAN_BAR                    1023
#define IDC_SCAN_BUSY                   1024
#define IDC_SCAN_IDLE                   1025
#define IDC_SCAN_WAIT                   1026
#define IDC_SCAN_RUN                    1027
#define IDC_SCAN_DONE                   1028
#define IDC_BRAND_IDLE                  1029
#define IDC_BRAND_WAIT                  1030
#define IDC_BRAND_RUN                   1031
#define IDC_BRAND_DONE                  1032
#define IDC_CALIB_IDLE                  1033
#define IDC_CALIB_WAIT                  1034
#define IDC_CALIB_RUN                   1035
#define IDC_CALIB_DONE                  1036
#define IDC_DEVICE_IDLE                 1037
#define IDC_DEVICE_IDENTIFY             1038
#define IDC_DEVICE_STANDBY              1039
#define IDC_DEVICE_SCAN                 1040
#define IDC_DEVICE_LOAD                 1041
#define IDC_DEVICE_BRAND                1042
#define IDC_DEVICE_CALIB                1043
#define IDC_SENS_PAPER1                 1045
#define IDC_SENS_PAPER2                 1046
#define IDC_SENS_PAPER3                 1047
#define IDC_SENS_PAPER4                 1048
#define IDC_SENS_TPH                    1049
#define IDC_SENS_EXIT                   1049
#define IDC_SENS_BRAND                  1050
#define IDC_SENS_TWOMEDIA               1051
#define IDC_SENS_COVER                  1052
#define IDC_TOP_MARGIN                  1053
#define IDC_BOTTOM_MARGIN               1054
#define IDC_IN_DOC_ON                   1055
#define IDC_IN_DOC_OFF                  1056
#define IDC_TPH_DOC_ON                  1057
#define IDC_TPH_DOC_OFF                 1058
#define IDC_INJECT_DOC_ON               1059
#define IDC_INJECT_DOC_OFF              1060
#define IDC_DOC_FULL_ON                 1061
#define IDC_DELAY_MARGIN                1062
#define IDC_HOME_ON                     1063
#define IDC_HOME_OFF                    1064
#define IDC_TOW_PAPER                   1065
#define IDC_BRAND_CHECK                 1066
#define IDC_DACR_VALUE                  1067
#define IDC_GAIN                        1068
#define IDC_BRAND_NONE                  1069
#define IDC_BRAND_FORWARD               1070
#define IDC_BRAND_BACKWARD              1071
#define IDC_INJECT_FRONT                1072
#define IDC_INJECT_BACK                 1073
#define IDC_AUTO_SCAN                   1074
#define IDC_CALIB_STEP                  1075
#define IDC_SET_SERIAL                  1076
#define IDC_SERIAL_NUMBER               1077
#define IDC_OMR_OUT                     1078
#define IDC_RESET                       1079
#define IDC_FACTORY_INIT                1080
#define IDC_CALIBRATION                 1081
#define IDC_DEBUG_MODE                  1082
#define IDC_INIT                        1083
#define IDC_FW_UPDATE                   1084
#define IDC_FORWARD                     1085
#define IDC_BACKWARD                    1086
#define IDC_FUNCTION                    1087
#define IDC_BRAND                       1088
#define IDC_SCAN_OMR                    1089
#define IDC_OMR_STR                     1090

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1097
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
