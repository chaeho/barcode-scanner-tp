// PreView.cpp : implementation file
//

#include "stdafx.h"
#include "ATECTP.h"
#include "ImageView.h"


// CImageView

IMPLEMENT_DYNAMIC(CImageView, CWnd)

CImageView::CImageView(CWnd* pParent)
{
	m_pData = NULL;
	m_nWidth = 0;
	m_nHeight = 0;
	//	m_pHstView = NULL;
}

CImageView::~CImageView()
{
}

BEGIN_MESSAGE_MAP(CImageView, CWnd)
	//{{AFX_MSG_MAP(CImageView)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CImageView::Make8bpp(BYTE* pData, int nWidth, int nHeight, BYTE* pImage, int offset, int stride)
{
	int x, y;

	for (y = 0; y < nHeight; y++)
	{
		for (x = 0; x < stride; x++)
		{
			pImage[(nHeight - 1 - y) * nWidth + x] = pData[y * stride + offset + x];
		}
	}
}

void CImageView::Make4bpp(BYTE* pData, int nWidth, int nHeight, BYTE* pImage, int offset, int stride)
{
	int	x, y;
	int	i, j;
	int	a, b;

	for (y = 0; y < nHeight; y++)
	{
		i = 0;
		for (x = 0; x < nWidth; x += 2)
		{
			a =  pData[(offset + y) * stride + i];
			for (j = 0; j < 2; j++)
			{
				switch (j)
				{
				case 1 :
					b = a & 0xf;
					break;

				case 0 :
					b = (a>>4) & 0xf;
					break;
				}
				pImage[(nHeight - 1 - y) * nWidth + x + j] = (b << 4) | b;
				//pImage[(nHeight - 1 - y)*nWidth + (nWidth-1-x) + j] = (b<<4) | b;
				//pImage[y*nWidth + x + j] = (b<<4) | b;
			}
			i++;
		}
	}
}


void CImageView::Make1bpp(BYTE* pData, int nWidth, int nHeight, BYTE* pImage, int offset, int stride)
{
	int	x, y;
	int	i, j;
	int	a, b;

	for (y = 0; y < nHeight; y++)
	{
		i = 0;
		for (x = 0; x < nWidth; x += 8)
		{
			a = pData[(offset + y) * stride + i];

			for (j = 0; j < 8; j++)
			{
				switch(j)
				{
				case 7:
					b = (a & 0x01) ? 0xff : 0x00;
					break;

				case 6:
					b = (a & 0x02) ? 0xff : 0x00;
					break;

				case 5:
					b = (a & 0x04) ? 0xff : 0x00;
					break;

				case 4:
					b = (a & 0x08) ? 0xff : 0x00;
					break;

				case 3:
					b = (a & 0x10) ? 0xff : 0x00;
					break;

				case 2:
					b = (a & 0x20) ? 0xff : 0x00;
					break;

				case 1:
					b = (a & 0x40) ? 0xff : 0x00;
					break;

				case 0:
					b = (a & 0x80) ? 0xff : 0x00;
					break;
				}
				pImage[(nHeight - 1 - y) * nWidth + x + j] = b;
				//pImage[(nHeight - 1 - y)*nWidth + (nWidth-1- x) + j] = b;
				//pImage[y*nWidth + x + j] = b;
			}
			i++;
		}
	}
}

// CImageView message handlers
void  CImageView::SetImage(BYTE* pData, int nWidth, int nHeight, int nPxlFmt)
{
	int		nLen;
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	int w = ((nWidth * 8 + 31) & ~31) / 8;
	int w1 = ((w * nPxlFmt + 31) & ~31) / 8;

	if (m_pData != NULL)
	{
		delete m_pData;
	}

	nLen = w * nHeight;
	m_pData = (BYTE *)new BYTE[nLen];

	switch (nPxlFmt)
	{
	case 8:
		Make8bpp(pData, w, nHeight, m_pData, 0, w1);
		break;

	case 4:
		Make4bpp(pData, w, nHeight, m_pData, 0, w1);
		break;

	case 1:
		Make1bpp(pData, w, nHeight, m_pData, 0, w1);
		break;
	} 
}

void CImageView::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if (m_pData != NULL)
	{
		CRect	rect;
		GetClientRect(&rect);

		BITMAPINFO*	pBm_Info = (BITMAPINFO *)(new BYTE[14 + 40 + 4 * 256]);
		pBm_Info->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		pBm_Info->bmiHeader.biWidth = m_nWidth;
		pBm_Info->bmiHeader.biHeight = m_nHeight;
		pBm_Info->bmiHeader.biPlanes = 1;
		pBm_Info->bmiHeader.biBitCount = 8;
		pBm_Info->bmiHeader.biCompression = BI_RGB;
		pBm_Info->bmiHeader.biSizeImage = m_nWidth*m_nHeight;
		pBm_Info->bmiHeader.biXPelsPerMeter = 0;
		pBm_Info->bmiHeader.biYPelsPerMeter = 0;
		pBm_Info->bmiHeader.biClrUsed = 256;
		pBm_Info->bmiHeader.biClrImportant = 256;
		
		for (int n = 0; n < 256; n++)
		{
			pBm_Info->bmiColors[n].rgbBlue = (BYTE)n;
			pBm_Info->bmiColors[n].rgbGreen = (BYTE)n;
			pBm_Info->bmiColors[n].rgbRed =  (BYTE)n;
			pBm_Info->bmiColors[n].rgbReserved = 255;
		}
		
		dc.SetStretchBltMode(HALFTONE);

		::StretchDIBits(dc.GetSafeHdc(),
			0,
			0,
			rect.Width(),
			rect.Height(),
			0,
			0,
			m_nWidth,
			m_nHeight,
			m_pData, 
			pBm_Info,
			DIB_RGB_COLORS,
			SRCCOPY);
		delete pBm_Info;
	}
}

BOOL CImageView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}
