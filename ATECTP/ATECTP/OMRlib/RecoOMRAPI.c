//************************************************
//*  OMR Interface DLL 32 Bits functions         *
//*  Library Version 2.0                         *
//*  Copyright 2009 - 2019 All rigths reserved   *
//*  RECOGNIFORM TECHNOLGIES SPA                 *
//*  www.recogniform.com - info@recogniform.com  *
//************************************************

#include "stdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

//  Goal: initialize the engine
//  Requires: company name, license string
//  Result: session handle
typedef long (__stdcall *OMR_INIT) (char*, char*);
OMR_INIT OMR_Init;


//  Goal: deinitialize the engine
//  Requires: session handle
//  Result: n/a
typedef void (__stdcall*OMR_DONE) (long);
OMR_DONE OMR_Done;


//  Goal: recognize a check box
//  Requires: session handle, dib handle, mark coordinates
//  Result: success or error
typedef long (__stdcall *OMR_RECOGNIZE) (long, long,long, long,long, long);
OMR_RECOGNIZE OMR_Recognize;


//  Goal: retrieve the percentage of ink
//  Requires: session handle
//  Result: success or error, the percentage of black pixels
typedef long (__stdcall *OMR_GETMARKDENSITYPERCENT) (long, double*);
OMR_GETMARKDENSITYPERCENT OMR_GetMarkDensityPercent;


//  Goal: the size of mark in percentage respect to the box diagonal size
//  Requires: session handle
//  Result: success or error, the size of mark in percentage
typedef long (__stdcall *OMR_GETMARKSIZEPERCENT) (long, double*);
OMR_GETMARKSIZEPERCENT OMR_GetMarkSizePercent;


//  Goal: load an image from file getting a DIB handle
//  Requires: session handle, file name
//  Result: the dib handle
typedef long (__stdcall *OMR_LOADIMAGE) (long, char*);
OMR_LOADIMAGE OMR_LoadImage;

//  Goal: free from memory an image previously loaded
//  Requires: session handle, DIB handle
//  Result: n/a
typedef void (__stdcall*OMR_FREEIMAGE) (long,long);
OMR_FREEIMAGE OMR_FreeImage;


//  Goal: import a DDB building an DIB handle.
//  Requires: session handle, PaletteHandle
//  Result: BitmapHandle
typedef long (__stdcall*OMR_IMPORTDDB) (long,long,long);
OMR_IMPORTDDB OMR_ImportDDB;

//  Goal: recognize if there is a signature on a part of document
//  Requires: session handle, DIB handle
//  Result: success or error, signature coordinates, stroke length, signature pixel of located printed character, number of the pixel of recognized horizontal line, noise in pixel
typedef long (__stdcall*OMR_RECOGNIZESIGNATURE) (long,long,long*,long*,long*,long*,long*,long*,long*,long*,long*);
OMR_RECOGNIZESIGNATURE OMR_RecognizeSignature;


// OCR Library handle
HINSTANCE OMRLIB=NULL;

long LoadOMRLibrary(void)
{
	OMRLIB = LoadLibrary(".\\RECOOMR.DLL");
	if(OMRLIB == NULL)
	{
		//MessageBox(NULL, "Unable to load RECOOMR.DLL", "ERROR", MB_OK);
		return -1;
	}
	else
	{
		OMR_Init=(OMR_INIT)GetProcAddress(OMRLIB,"OMR_Init");
		OMR_Done=(OMR_DONE)GetProcAddress(OMRLIB,"OMR_Done");
		OMR_Recognize=(OMR_RECOGNIZE)GetProcAddress(OMRLIB,"OMR_Recognize");
		OMR_GetMarkDensityPercent=(OMR_GETMARKDENSITYPERCENT)GetProcAddress(OMRLIB,"OMR_GetMarkDensityPercent");
		OMR_GetMarkSizePercent=(OMR_GETMARKSIZEPERCENT)GetProcAddress(OMRLIB,"OMR_GetMarkSizePercent");
		OMR_ImportDDB=(OMR_IMPORTDDB)GetProcAddress(OMRLIB,"OMR_ImportDDB");
		OMR_LoadImage=(OMR_LOADIMAGE)GetProcAddress(OMRLIB,"OMR_LoadImage");
		OMR_FreeImage=(OMR_FREEIMAGE)GetProcAddress(OMRLIB,"OMR_FreeImage");
		OMR_RecognizeSignature=(OMR_RECOGNIZESIGNATURE)GetProcAddress(OMRLIB,"OMR_RecognizeSignature");
		return 0;
	}
}


// Use this funciton to unload the OMR library when you don't need it
void FreeOMRLibrary(void)
{
	if (OMRLIB != NULL)
	{
		FreeLibrary(OMRLIB);
		OMRLIB = NULL;
	}
}
