
// ATECTPDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ATECTP.h"
#include "ATECTPDlg.h"
#include "afxdialogex.h"

#include <direct.h>
#include "AtecScanLib.h"
#include "BmpStruct.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma warning(disable : 4996)

#define IMAGE_WIDTH_PIXEL                       (832)
#define IMAGE_MAX_LINE                          (2500)
#define IMAGE_BIT_PER_PIXEL                     (8)

#define SENS_CH_MASK_PAPER_1                    (1 << 0)
#define SENS_CH_MASK_PAPER_2                    (1 << 1)
#define SENS_CH_MASK_EXIT                       (1 << 2)
#define SENS_CH_MASK_PAPER_3                    (1 << 8)
#define SENS_CH_MASK_COVER                      (1 << 9)
#define SENS_CH_MASK_PAPER_4                    (1 << 10)
#define SENS_CH_MASK_TPH                        (1 << 11)
#define SENS_CH_MASK_DOUBLE_MEDIA               (1 << 12)

#define SCAN_IMAGE_FILE_PATH                    ("C://ATEC T&//front.bmp")



// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CATECTPDlg 대화 상자
CATECTPDlg::CATECTPDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CATECTPDlg::IDD, pParent)
{
	m_hIcon     = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bUSBOpen  = FALSE;
    m_font      = NULL;
    m_fontSize  = 0;

	// Calibration step 변수 초기화
	m_step		= CALIB_STEP_NONE;
}

void CATECTPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CATECTPDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_USB_CLOSE, &CATECTPDlg::OnBnClickedUsbClose)
	ON_BN_CLICKED(IDC_USB_OPEN, &CATECTPDlg::OnBnClickedUsbOpen)
	ON_BN_CLICKED(IDC_SCAN_START, &CATECTPDlg::OnBnClickedScanStart)
	ON_BN_CLICKED(IDC_SCAN_STOP, &CATECTPDlg::OnBnClickedScanStop)
	ON_BN_CLICKED(IDC_MOTOR_START, &CATECTPDlg::OnBnClickedMotorStart)
	ON_BN_CLICKED(IDC_MOTOR_STOP, &CATECTPDlg::OnBnClickedMotorStop)
	ON_BN_CLICKED(IDC_LED_ON_OFF, &CATECTPDlg::OnBnClickedLedOnOff)
	ON_BN_CLICKED(IDC_GET_PARAMETER, &CATECTPDlg::OnBnClickedGetParameter)
	ON_BN_CLICKED(IDC_SET_PARAMETER, &CATECTPDlg::OnBnClickedSetParameter)
	ON_BN_CLICKED(IDC_SET_SERIAL, &CATECTPDlg::OnBnClickedSetSerial)
	ON_BN_CLICKED(IDC_RESET, &CATECTPDlg::OnBnClickedReset)
	ON_BN_CLICKED(IDC_CALIBRATION, &CATECTPDlg::OnBnClickedCalibration)
	ON_BN_CLICKED(IDC_FACTORY_INIT, &CATECTPDlg::OnBnClickedFactoryInit)
	ON_BN_CLICKED(IDC_FW_UPDATE, &CATECTPDlg::OnBnClickedFwUpdate)
    ON_BN_CLICKED(IDC_FONT, &CATECTPDlg::OnBnClickedFont)
    ON_BN_CLICKED(IDC_FONT_DOWN, &CATECTPDlg::OnBnClickedFontDown)
END_MESSAGE_MAP()


// CATECTPDlg 메시지 처리기
BOOL CATECTPDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	m_pImageView_front = new CImageView(this);
	CRect rect_front(10, 270, 310, 780);

	m_pImageView_front->Create(NULL, NULL, WS_CHILD | WS_VISIBLE, rect_front, this, 3003);
	m_pImageView_front->m_nWidth    = IMAGE_WIDTH_PIXEL;
	m_pImageView_front->m_nHeight   = IMAGE_MAX_LINE;
    m_pImageView_front->m_pData     = NULL;
	m_pImageView_front->ShowWindow(SW_SHOW);

    /* OMR 좌표 출력 폰트 변경 (기본 폰트는 폭이 일정치 않음) */
    CFont font;
    font.CreatePointFont(100, "Consolas");
    GetDlgItem(IDC_OMR_STR)->SetFont(&font);
    font.Detach();

	InitWindows();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CATECTPDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.
void CATECTPDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CATECTPDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BYTE CATECTPDlg::BinToBcd(BYTE Bin)
{
	return (((Bin / 10) << 4) + (Bin % 10));
}

void CATECTPDlg::GetTimeBCD(BYTE* BCDTime)
{
	CTime tm;
	BYTE time_sync[7];

	tm = CTime::GetCurrentTime();
	time_sync[0] = (BYTE)(tm.GetYear() / 100);
	time_sync[1] = (BYTE)(tm.GetYear() % 100);
	time_sync[2] = (BYTE)tm.GetMonth();
	time_sync[3] = (BYTE)tm.GetDay();
	time_sync[4] = (BYTE)tm.GetHour();
	time_sync[5] = (BYTE)tm.GetMinute();
	time_sync[6] = (BYTE)tm.GetSecond();

	for (int i = 0; i < 7; i++)
	{
		BCDTime[i] = BinToBcd(time_sync[i]);
	}
}

void CATECTPDlg::InitWindows()
{
	if (m_bUSBOpen)
	{
        GetDlgItem(IDC_SCAN_BAR)->EnableWindow(TRUE);
        GetDlgItem(IDC_SCAN_OMR)->EnableWindow(TRUE);
        ((CButton*)GetDlgItem(IDC_SCAN_BAR))->SetCheck(1);
        ((CButton*)GetDlgItem(IDC_SCAN_OMR))->SetCheck(0);

		GetDlgItem(IDC_SCAN_START)->EnableWindow(TRUE);
		GetDlgItem(IDC_SCAN_STOP)->EnableWindow(TRUE);
		GetDlgItem(IDC_MOTOR_START)->EnableWindow(TRUE);
		GetDlgItem(IDC_MOTOR_STOP)->EnableWindow(TRUE);
		GetDlgItem(IDC_MOTOR1)->EnableWindow(TRUE);
		GetDlgItem(IDC_MOTOR2)->EnableWindow(TRUE);

		((CButton*)GetDlgItem(IDC_MOTOR2))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_MOTOR1))->SetCheck(1);

		GetDlgItem(IDC_CW)->EnableWindow(TRUE);
		GetDlgItem(IDC_CCW)->EnableWindow(TRUE);

		((CButton*)GetDlgItem(IDC_CCW))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_CW))->SetCheck(1);

		GetDlgItem(IDC_INJECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_REJECT)->EnableWindow(TRUE);
		((CButton*)GetDlgItem(IDC_REJECT))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_INJECT))->SetCheck(1);
	
		GetDlgItem(IDC_INJECT_FRONT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INJECT_BACK)->EnableWindow(FALSE);
		((CButton*)GetDlgItem(IDC_INJECT_BACK))->SetCheck(1);
		((CButton*)GetDlgItem(IDC_INJECT_FRONT))->SetCheck(0);

		GetDlgItem(IDC_BRAND_NONE)->EnableWindow(TRUE);
		GetDlgItem(IDC_BRAND_FORWARD)->EnableWindow(TRUE);
		((CButton*)GetDlgItem(IDC_BRAND_BACKWARD))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_BRAND_FORWARD))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_BRAND_NONE))->SetCheck(1);
        GetDlgItem(IDC_FONT)->EnableWindow(TRUE);
        GetDlgItem(IDC_FONT_DOWN)->EnableWindow(TRUE);

		GetDlgItem(IDC_LED_ON_OFF)->EnableWindow(TRUE);

		GetDlgItem(IDC_GET_PARAMETER)->EnableWindow(TRUE);
		GetDlgItem(IDC_SET_PARAMETER)->EnableWindow(TRUE);

		GetDlgItem(IDC_TOP_MARGIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_BOTTOM_MARGIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_IN_DOC_ON)->EnableWindow(TRUE);
		GetDlgItem(IDC_IN_DOC_OFF)->EnableWindow(TRUE);
		GetDlgItem(IDC_TPH_DOC_ON)->EnableWindow(TRUE);
		GetDlgItem(IDC_TPH_DOC_OFF)->EnableWindow(TRUE);
		GetDlgItem(IDC_INJECT_DOC_ON)->EnableWindow(TRUE);
		GetDlgItem(IDC_INJECT_DOC_OFF)->EnableWindow(TRUE);
		GetDlgItem(IDC_DOC_FULL_ON)->EnableWindow(TRUE);
		GetDlgItem(IDC_DELAY_MARGIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_HOME_ON)->EnableWindow(TRUE);
		GetDlgItem(IDC_HOME_OFF)->EnableWindow(TRUE);
		GetDlgItem(IDC_TOW_PAPER)->EnableWindow(TRUE);
		GetDlgItem(IDC_BRAND_CHECK)->EnableWindow(TRUE);
		GetDlgItem(IDC_DACR_VALUE)->EnableWindow(TRUE);
		GetDlgItem(IDC_GAIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_RESET)->EnableWindow(TRUE);
		GetDlgItem(IDC_CALIBRATION)->EnableWindow(TRUE);
		GetDlgItem(IDC_FACTORY_INIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_SET_SERIAL)->EnableWindow(TRUE);
	}
	else
	{
        GetDlgItem(IDC_SCAN_BAR)->EnableWindow(FALSE);
        GetDlgItem(IDC_SCAN_OMR)->EnableWindow(FALSE);

		GetDlgItem(IDC_SCAN_START)->EnableWindow(FALSE);
		GetDlgItem(IDC_SCAN_STOP)->EnableWindow(FALSE);
		GetDlgItem(IDC_MOTOR_START)->EnableWindow(FALSE);
		GetDlgItem(IDC_MOTOR_STOP)->EnableWindow(FALSE);
		GetDlgItem(IDC_MOTOR1)->EnableWindow(FALSE);
		GetDlgItem(IDC_MOTOR2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CCW)->EnableWindow(FALSE);

		GetDlgItem(IDC_INJECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_REJECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INJECT_FRONT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INJECT_BACK)->EnableWindow(FALSE);
		GetDlgItem(IDC_BRAND_NONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_BRAND_FORWARD)->EnableWindow(FALSE);
		GetDlgItem(IDC_BRAND_BACKWARD)->EnableWindow(FALSE);

        GetDlgItem(IDC_FONT)->EnableWindow(FALSE);
        GetDlgItem(IDC_FONT_DOWN)->EnableWindow(FALSE);

		GetDlgItem(IDC_LED_ON_OFF)->EnableWindow(FALSE);

		GetDlgItem(IDC_GET_PARAMETER)->EnableWindow(FALSE);
		GetDlgItem(IDC_SET_PARAMETER)->EnableWindow(FALSE);

		GetDlgItem(IDC_TOP_MARGIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BOTTOM_MARGIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_IN_DOC_ON)->EnableWindow(FALSE);
		GetDlgItem(IDC_IN_DOC_OFF)->EnableWindow(FALSE);
		GetDlgItem(IDC_TPH_DOC_ON)->EnableWindow(FALSE);
		GetDlgItem(IDC_TPH_DOC_OFF)->EnableWindow(FALSE);
		GetDlgItem(IDC_INJECT_DOC_ON)->EnableWindow(FALSE);
		GetDlgItem(IDC_INJECT_DOC_OFF)->EnableWindow(FALSE);
		GetDlgItem(IDC_DOC_FULL_ON)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELAY_MARGIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_HOME_ON)->EnableWindow(FALSE);
		GetDlgItem(IDC_HOME_OFF)->EnableWindow(FALSE);
		GetDlgItem(IDC_TOW_PAPER)->EnableWindow(FALSE);
		GetDlgItem(IDC_BRAND_CHECK)->EnableWindow(FALSE);
		GetDlgItem(IDC_DACR_VALUE)->EnableWindow(FALSE);
		GetDlgItem(IDC_GAIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_RESET)->EnableWindow(FALSE);
		GetDlgItem(IDC_CALIBRATION)->EnableWindow(FALSE);
		GetDlgItem(IDC_FACTORY_INIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_SET_SERIAL)->EnableWindow(FALSE);
	}
}

void CATECTPDlg::OnBnClickedUsbOpen()
{
	CString str;

	UpdateData(TRUE);

	ATEC_DeviceClose();

    if (ATEC_DeviceOpen(((CButton *)GetDlgItem(IDC_DEBUG_MODE))->GetCheck()) == 0)
	{
        unsigned char ver[4];
        unsigned char bSerial[33];

        mkdir("C://ATEC T&");

		ATEC_Reset(0);
		ATEC_GetFirmwareVersion(&ver[0]);

        /* 메인타이틀에 스캐너 펌웨어 버전 출력 */
        str.Format("ATECTP version %c.%c%c%c", ver[0], ver[1], ver[2], ver[3]);
		SetWindowText(str);		
		
		ATEC_GetSerial(&bSerial[0]);
        str.Format("%s\n", bSerial);
        ((CEdit *)GetDlgItem(IDC_SERIAL_NUMBER))->SetWindowTextA(str);

		m_bUSBOpen = TRUE;

		OnPoolStart();
		InitWindows();
	}
	else
	{
		str.Format("open fail\n");
		OnMasData(str);
		m_bUSBOpen = FALSE;
	}

	UpdateData(FALSE);
}

void CATECTPDlg::OnBnClickedUsbClose()
{
	OnPoolStop();

	if (m_bUSBOpen)
	{
		ATEC_DeviceClose();
		OnMasData("\r\nclose device.");
	}

	m_bUSBOpen = FALSE;
	InitWindows();
}

void CATECTPDlg::OnPoolStart() 
{
	SetTimer(1, 100, NULL);
}

void CATECTPDlg::OnPoolStop() 
{
	KillTimer(1);
}

void CATECTPDlg::OnMasData(CString msg)
{
    ((CEdit *)GetDlgItem(IDC_PACKET_STR))->SetWindowText(msg);
}

void CATECTPDlg::OnTimer(UINT nIDEvent) 
{
	CString		    Packet;
	CString         Row;
	CString         OMR;
	unsigned char* FrontRawBuf;
	unsigned char* DummyBuf;
	unsigned char* pOMRBitmap;
	int			    FileSize;

	switch (nIDEvent) {
	case 1:
		OnMasData(Packet);

		if (m_bUSBOpen == TRUE)
		{
			if (ATEC_IsDevice() != 0)
			{
				OnBnClickedUsbClose();
				return;
			}

			int ret, scanline;

			BYTE bstatus[14];

			ret = ATEC_GetStatus(&bstatus[0]);
			if (ret == 0)
			{
				Check_Status((char*)bstatus);

				Packet.Format("status[%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x]",
					bstatus[0], bstatus[1], bstatus[2], bstatus[3], bstatus[4], bstatus[5], bstatus[6], bstatus[7],
					bstatus[8], bstatus[9], bstatus[10], bstatus[11], bstatus[12], bstatus[13], bstatus[14], bstatus[15]);
				OnMasData(Packet);

				/* scan state & scan done */
				if ((bstatus[0] == 0x03) && (bstatus[1] == 0x03))
				{
					CEdit* pEdit;
					pEdit = (CEdit*)GetDlgItem(IDC_OMR_STR);

					pEdit->SetSel(0, -1);
					pEdit->Clear();

					scanline = bstatus[10] | (bstatus[11] << 8);

					if (((CButton*)GetDlgItem(IDC_SCAN_OMR))->GetCheck() == BST_CHECKED)
					{
						pOMRBitmap = new unsigned char[64];

						ATEC_SendOMR(pOMRBitmap);
						unsigned short* pLineBitmap = (unsigned short*)pOMRBitmap;

						OMR.Format("    F E D C B A 9 8 7 6 5 4 3 2 1 0\r\n");

						for (int LineNum = 0; LineNum < NUM_OMR_LINE; LineNum++)
						{
							Row.Format("%02d     ", LineNum + 1);
							OMR += Row;

							for (int MarkerNum = 0; MarkerNum < NUM_MARKER_PER_LINE; MarkerNum++)
							{
								if ((pLineBitmap[LineNum] & (1 << MarkerNum)) > 0)
								{
									OMR += " =";
								}
								else
								{
									OMR += "  ";
								}
							}

							OMR += "\r\n";
						}

						pEdit->SetWindowTextA(OMR);
						delete(pOMRBitmap);
					}

					OnPoolStop();

					/* raw data image를 파일로 파일로 저장 */
					FileSize = scanline * IMAGE_WIDTH_PIXEL;

					FrontRawBuf = new unsigned char[FileSize];
					DummyBuf = new unsigned char[FileSize];

					ATEC_ScanBuf(scanline, FrontRawBuf, DummyBuf);
					BarCodeRead(FrontRawBuf, 1, scanline);

					/* 용지 후처리 결정 */
					if (((CButton*)GetDlgItem(IDC_INJECT))->GetCheck() == BST_CHECKED)
					{
						if (((CButton*)GetDlgItem(IDC_INJECT_FRONT))->GetCheck() == BST_CHECKED)
						{
							ATEC_Inject(0);
						}
						else if (((CButton*)GetDlgItem(IDC_INJECT_BACK))->GetCheck() == BST_CHECKED)
						{
							ATEC_Inject(1);
						}
					}
					else if (((CButton*)GetDlgItem(IDC_REJECT))->GetCheck() == BST_CHECKED)
					{
						ATEC_Reject();
					}

					if (((CButton*)GetDlgItem(IDC_AUTO_SCAN))->GetCheck() == BST_CHECKED)
					{
						if (((CButton*)GetDlgItem(IDC_SCAN_OMR))->GetCheck() == BST_CHECKED)
						{
							ATEC_ScannerOn(1);
						}
						else
						{
							ATEC_ScannerOn(0);
						}
					}

					/* TP에 이미지 재생 */
					if (scanline != 0)
					{
						m_pImageView_front->SetImage((BYTE*)FrontRawBuf, IMAGE_WIDTH_PIXEL, scanline, 8);
						m_pImageView_front->Invalidate(FALSE);

						ATEC_SaveBMP(IMAGE_WIDTH_PIXEL, scanline, FrontRawBuf, SCAN_IMAGE_FILE_PATH);
					}

					delete FrontRawBuf;
					delete DummyBuf;

					OnPoolStart();
				}

				/* calibration done */
				if ((bstatus[0] == 0x06) && (bstatus[3] == 0x05))
				{
					MessageBox(_T("CALIBRATION이 완료되었습니다. RESET 버튼을 클릭하여 재시작하세요"), _T("CALIBRATION COMPLETE!"));
				}
			}
		}
		break;
	case 2:
		Calibration_Step(CALIB_STEP_ABORT);
		OnCalibTimerStop();
		break;
	}

}


void CATECTPDlg::BarCodeRead(unsigned char *rawfile, int TopOrBottom, int height) 
{
	CString			str;
	char			barcodedata[128] = {0,};
	int				barcode_type;
	unsigned short	LastPosY;

	barcode_type = ATEC_Barcode(rawfile, IMAGE_WIDTH_PIXEL, height, barcodedata, &LastPosY);
	UpdateData(TRUE);

	if (barcode_type > 0)
	{
		str.Format("%s, %d", barcodedata, barcode_type);
        ((CEdit *)GetDlgItem(IDC_BARCODE_STR))->SetWindowTextA(str);

        if (((CButton*)GetDlgItem(IDC_BRAND_FORWARD))->GetCheck() == BST_CHECKED)
        {
            ATEC_TPHBranding(0, LastPosY);
        }
	}
	else
	{
		if (TopOrBottom == 1)
		{
			str.Format("Barcode Front Fail %d", barcode_type);
            ((CEdit *)GetDlgItem(IDC_BARCODE_STR))->SetWindowTextA(str);
		}
		else
		{
			str.Format("Barcode Back Fail %d", barcode_type);
		}
	}

	UpdateData(FALSE);
}

void CATECTPDlg::OnBnClickedScanStart()
{
	OnPoolStop();

    if (((CButton*)GetDlgItem(IDC_SCAN_OMR))->GetCheck() == BST_CHECKED)
    {
        ATEC_ScannerOn(1);
    }
    else
    {
        ATEC_ScannerOn(0);
    }
	
	OnPoolStart();
}


void CATECTPDlg::OnBnClickedScanStop()
{
	OnPoolStop();
	ATEC_ScannerOff();
	OnPoolStart();
}

void CATECTPDlg::OnBnClickedMotorStart()
{
	char MotorType;
	BOOL Dir;

	if (((CButton*)GetDlgItem(IDC_MOTOR1))->GetCheck() == BST_CHECKED)
	{
		MotorType = 0;
	}
	else if (((CButton*)GetDlgItem(IDC_MOTOR2))->GetCheck() == BST_CHECKED)
	{
		MotorType = 1;
	}

	if (((CButton*)GetDlgItem(IDC_CW))->GetCheck() == BST_CHECKED)
	{
		Dir = 0;
	}
	else if (((CButton*)GetDlgItem(IDC_CCW))->GetCheck() == BST_CHECKED)
	{
		Dir = 1;
	}

	ATEC_MotorStart(MotorType, Dir);
}

void CATECTPDlg::OnBnClickedMotorStop()
{
	char MotorType;

	if (((CButton*)GetDlgItem(IDC_MOTOR1))->GetCheck() == BST_CHECKED)
	{
		MotorType = 0;
	}
	else if (((CButton*)GetDlgItem(IDC_MOTOR2))->GetCheck() == BST_CHECKED)
	{
		MotorType = 1;
	}

	ATEC_MotorStop(MotorType);
}

int CATECTPDlg::BigToWord(BYTE *pbSrcBig)
{
	int Res = 0;

	Res = pbSrcBig[0] << 24 | pbSrcBig[1] << 16 | pbSrcBig[2] << 8 | pbSrcBig[3];
	return Res;
}

int CATECTPDlg::WordToBig(int wSrc, BYTE *pbDestBig)
{
	BYTE *p;

	p = (BYTE*)&wSrc;

	pbDestBig[0] = *(p + 3);
	pbDestBig[1] = *(p + 2);
	pbDestBig[2] = *(p + 1);
	pbDestBig[3] = *p;

	return 4;
}

void CATECTPDlg::SENSOR_Check(unsigned short status)
{
    UpdateData(TRUE);

    ((CButton*)GetDlgItem(IDC_SENS_PAPER1))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_PAPER2))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_PAPER3))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_PAPER4))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_TPH))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_BRAND))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_TWOMEDIA))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_SENS_COVER))->SetCheck(0);


    if (status & SENS_CH_MASK_PAPER_1)
    {
        ((CButton*)GetDlgItem(IDC_SENS_PAPER1))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_PAPER_2)
    {
        ((CButton*)GetDlgItem(IDC_SENS_PAPER2))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_PAPER_3)
    {
        ((CButton*)GetDlgItem(IDC_SENS_PAPER3))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_PAPER_4)
    {
        ((CButton*)GetDlgItem(IDC_SENS_PAPER4))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_EXIT)
    {
        ((CButton*)GetDlgItem(IDC_SENS_TPH))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_TPH)
    {
        ((CButton*)GetDlgItem(IDC_SENS_BRAND))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_DOUBLE_MEDIA)
    {
        ((CButton*)GetDlgItem(IDC_SENS_TWOMEDIA))->SetCheck(1);
    }

    if (status & SENS_CH_MASK_COVER)
    {
        ((CButton*)GetDlgItem(IDC_SENS_COVER))->SetCheck(1);
    }

    UpdateData(FALSE);
}

void CATECTPDlg::Calib_Status(char status)
{	
	int res = 0;

	UpdateData(TRUE);

	((CButton*)GetDlgItem(IDC_CALIB_IDLE))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CALIB_WAIT))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CALIB_RUN))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CALIB_DONE))->SetCheck(0);

	switch (status)
	{
	case 0:
		((CButton*)GetDlgItem(IDC_CALIB_IDLE))->SetCheck(1);
		switch (m_step) {
		case CALIB_STEP_ABORT:
			MessageBox(_T("Calibration이 중단되었습니다."), _T("Calibration"), MB_OK);
			Calibration_Step(CALIB_STEP_NONE);
		}
		break;

	case 1:
		((CButton*)GetDlgItem(IDC_CALIB_WAIT))->SetCheck(1);
		break;

	case 2:
		((CButton*)GetDlgItem(IDC_CALIB_RUN))->SetCheck(1);
		break;

    case 3:
        ((CButton*)GetDlgItem(IDC_CALIB_DONE))->SetCheck(1);
		switch (m_step) {
		case CALIB_STEP_OUT:
			res = MessageBox(_T("OUT Calibration이 끝났습니다.\nIN Calibration 진행을 원하시면 '확인'\n중단을 원하시면 '취소'를 눌러주세요"), _T("Calibration"), MB_OKCANCEL);
			if (res == IDOK) {
				Calibration_Step(CALIB_STEP_IN);
			}
			else {
				Calibration_Step(CALIB_STEP_ABORT);
			}
			break;
		case CALIB_STEP_IN:
			OnCalibTimerStop();
			res = MessageBox(_T("IN Calibration을 다시 진행하시겠습니까?"), _T("Calibration"), MB_OKCANCEL);
			if (res == IDOK) {
				Calibration_Step(CALIB_STEP_IN);
			}
			else {
				res = MessageBox(_T("IN Calibration이 끝났습니다.\nDOUBLE Calibration 진행을 원하시면 '확인'\n중단을 원하시면 '취소'를 눌러주세요"), _T("Calibration"), MB_OKCANCEL);
				if (res == IDOK) {
					Calibration_Step(CALIB_STEP_DOUBLE);
				}
				else {
					Calibration_Step(CALIB_STEP_ABORT);
				}
			}
			break;
		case CALIB_STEP_DOUBLE:
			OnCalibTimerStop();
			res = MessageBox(_T("DOUBLE Calibration을 다시 진행하시겠습니까?"), _T("Calibration"), MB_OKCANCEL);
			if (res == IDOK) {
				Calibration_Step(CALIB_STEP_DOUBLE);
			}
			else {
				MessageBox(_T("Calibration이 끝났습니다."), _T("Calibration"), MB_OK);
				Calibration_Step(CALIB_STEP_NONE);
			}
			break;
		}
        break;
	}
	UpdateData(FALSE);
}

void CATECTPDlg::Brand_Status(char status)
{
	UpdateData(TRUE);

	((CButton*)GetDlgItem(IDC_BRAND_IDLE))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_BRAND_WAIT))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_BRAND_RUN))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_BRAND_DONE))->SetCheck(0);

	switch(status)
	{
	case 0:
		((CButton*)GetDlgItem(IDC_BRAND_IDLE))->SetCheck(1);
		break;

	case 1:
		((CButton*)GetDlgItem(IDC_BRAND_WAIT))->SetCheck(1);
		break;

	case 2:
		((CButton*)GetDlgItem(IDC_BRAND_RUN))->SetCheck(1);
		break;

	case 3:
		((CButton*)GetDlgItem(IDC_BRAND_DONE))->SetCheck(1);
		break;
	}
	UpdateData(FALSE);
}

void CATECTPDlg::Scan_Status(char status)
{
	UpdateData(TRUE);
	((CButton*)GetDlgItem(IDC_SCAN_IDLE))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_SCAN_WAIT))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_SCAN_RUN))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_SCAN_DONE))->SetCheck(0);

	switch(status)
	{
	case 0:
		((CButton*)GetDlgItem(IDC_SCAN_IDLE))->SetCheck(1);
		break;

	case 1:
		((CButton*)GetDlgItem(IDC_SCAN_WAIT))->SetCheck(1);
		break;

	case 2:
		((CButton*)GetDlgItem(IDC_SCAN_RUN))->SetCheck(1);
		break;

	case 3:
		((CButton*)GetDlgItem(IDC_SCAN_DONE))->SetCheck(1);
		break;

    case 4:
        ((CButton*)GetDlgItem(IDC_SCAN_BUSY))->SetCheck(1);
        break;
	}

	UpdateData(FALSE);
}

void CATECTPDlg::Device_Status(char status)
{
	UpdateData(TRUE);

	((CButton*)GetDlgItem(IDC_DEVICE_IDLE))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DEVICE_IDENTIFY))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DEVICE_STANDBY))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DEVICE_SCAN))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DEVICE_LOAD))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DEVICE_BRAND))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_DEVICE_CALIB))->SetCheck(0);

	switch(status)
	{
	case 0:
		((CButton*)GetDlgItem(IDC_DEVICE_IDLE))->SetCheck(1);
		break;

	case 1:
		((CButton*)GetDlgItem(IDC_DEVICE_IDENTIFY))->SetCheck(1);
		break;

	case 2:
		((CButton*)GetDlgItem(IDC_DEVICE_STANDBY))->SetCheck(1);
		break;

	case 3:
		((CButton*)GetDlgItem(IDC_DEVICE_SCAN))->SetCheck(1);
		break;

	case 4:
		((CButton*)GetDlgItem(IDC_DEVICE_LOAD))->SetCheck(1);
		break;

	case 5:
		((CButton*)GetDlgItem(IDC_DEVICE_BRAND))->SetCheck(1);
		break;

    case 6:
        ((CButton*)GetDlgItem(IDC_DEVICE_CALIB))->SetCheck(1);
        break;
	}
	UpdateData(FALSE);
}

void CATECTPDlg::Check_Status(char *status)
{
	static unsigned char    device_state                = 0;
	static unsigned char    scan_state                  = 0;
	static unsigned char    brand_state                 = 0;
	static unsigned char    calibration_state           = 0;
    static unsigned short   Sensor                      = 0;

	//memcpy(TempStatus, status, sizeof(TempStatus));

	UpdateData(TRUE);
	if (status[0] != device_state)
	{
		device_state = status[0];
		Device_Status(device_state);
	}

	if (status[1] != scan_state)
	{
		scan_state = status[1];
		Scan_Status(scan_state);
	}

	if (status[2] != brand_state)
	{
		brand_state = status[2];
		Brand_Status(brand_state);
	}

	if (status[3] != calibration_state)
	{
		calibration_state = status[3];
		Calib_Status(calibration_state);
	}

    if (memcmp(&status[4], &Sensor, sizeof(Sensor)) != 0)
    {
        Sensor  = status[4];
        Sensor |= status[5] << 8;

        SENSOR_Check(Sensor);
    }

	UpdateData(FALSE);
}

int CATECTPDlg::Calibration_Step(unsigned char step) 
{
	int res = 0;

	m_step = step;
	if (m_step) {
		res = ATEC_Calibration(m_step);

	}
	if (!res) {
		switch (m_step) {
		case CALIB_STEP_NONE:
			((CEdit*)GetDlgItem(IDC_CALIB_STEP))->SetWindowText("None");
			break;
		case CALIB_STEP_OUT:
			((CEdit*)GetDlgItem(IDC_CALIB_STEP))->SetWindowText("Out");
			break;
		case CALIB_STEP_IN:
			((CEdit*)GetDlgItem(IDC_CALIB_STEP))->SetWindowText("In");
			OnCalibTimerStart();
			break;
		case CALIB_STEP_DOUBLE:
			((CEdit*)GetDlgItem(IDC_CALIB_STEP))->SetWindowText("Double");
			OnCalibTimerStart();
			break;
		case CALIB_STEP_ABORT:
			((CEdit*)GetDlgItem(IDC_CALIB_STEP))->SetWindowText("Abort");
			break;
		}
	}
	return res;
}

void CATECTPDlg::OnCalibTimerStart()
{
	SetTimer(2, 10000, NULL);
}

void CATECTPDlg::OnCalibTimerStop() 
{
	KillTimer(2);
}

void CATECTPDlg::OnBnClickedLedOnOff()
{
	if (m_bLedON_OFF) {
		ATEC_LEDOff();
		m_bLedON_OFF = 0;
	} else {
		ATEC_LEDOn();
		m_bLedON_OFF = 1;
	}
}

void CATECTPDlg::OnBnClickedGetParameter()
{
    CString TempStr;
    BYTE    bGetParam[32];

	UpdateData(TRUE);

	ATEC_GetParam((BYTE*)&bGetParam);

    SetDlgItemInt(IDC_TOP_MARGIN,       bGetParam[0],  FALSE);
    SetDlgItemInt(IDC_BOTTOM_MARGIN,    bGetParam[1],  FALSE);
    SetDlgItemInt(IDC_IN_DOC_ON,        bGetParam[2],  FALSE);
    SetDlgItemInt(IDC_IN_DOC_OFF,       bGetParam[3],  FALSE);
    SetDlgItemInt(IDC_TPH_DOC_ON,       bGetParam[4],  FALSE);
    SetDlgItemInt(IDC_TPH_DOC_OFF,      bGetParam[5],  FALSE);
    SetDlgItemInt(IDC_INJECT_DOC_ON,    bGetParam[6],  FALSE);
    SetDlgItemInt(IDC_INJECT_DOC_OFF,   bGetParam[7],  FALSE);
    SetDlgItemInt(IDC_DOC_FULL_ON,      bGetParam[8],  FALSE);
    SetDlgItemInt(IDC_DELAY_MARGIN,     bGetParam[9],  FALSE);
    SetDlgItemInt(IDC_HOME_ON,          bGetParam[10], FALSE);
    SetDlgItemInt(IDC_HOME_OFF,         bGetParam[11], FALSE);
    SetDlgItemInt(IDC_TOW_PAPER,        bGetParam[12], FALSE);
    SetDlgItemInt(IDC_BRAND_CHECK,      bGetParam[13], FALSE);
    SetDlgItemInt(IDC_DACR_VALUE,       bGetParam[14], FALSE);
    SetDlgItemInt(IDC_GAIN,             bGetParam[15], FALSE);

	UpdateData(FALSE);
}

void CATECTPDlg::OnBnClickedSetParameter()
{
	BYTE bSetParam[32];
	memset(bSetParam, 0x00, sizeof(bSetParam));

	UpdateData(TRUE);

    bSetParam[0]  = GetDlgItemInt(IDC_TOP_MARGIN);
	bSetParam[1]  = GetDlgItemInt(IDC_BOTTOM_MARGIN);
	bSetParam[2]  = GetDlgItemInt(IDC_IN_DOC_ON);
	bSetParam[3]  = GetDlgItemInt(IDC_IN_DOC_OFF);
	bSetParam[4]  = GetDlgItemInt(IDC_TPH_DOC_ON);
	bSetParam[5]  = GetDlgItemInt(IDC_TPH_DOC_OFF);
	bSetParam[6]  = GetDlgItemInt(IDC_INJECT_DOC_ON);
	bSetParam[7]  = GetDlgItemInt(IDC_INJECT_DOC_OFF);
	bSetParam[8]  = GetDlgItemInt(IDC_DOC_FULL_ON);
	bSetParam[9]  = GetDlgItemInt(IDC_DELAY_MARGIN);
	bSetParam[10] = GetDlgItemInt(IDC_HOME_ON);
	bSetParam[11] = GetDlgItemInt(IDC_HOME_OFF);
	bSetParam[12] = GetDlgItemInt(IDC_TOW_PAPER);
	bSetParam[13] = GetDlgItemInt(IDC_BRAND_CHECK);
	bSetParam[14] = GetDlgItemInt(IDC_DACR_VALUE);
	bSetParam[15] = GetDlgItemInt(IDC_GAIN);

	ATEC_SetParam((BYTE*)bSetParam, sizeof(bSetParam));

	UpdateData(FALSE);
}

void CATECTPDlg::OnBnClickedSetSerial()
{
    CString         cSerialNum;
    char            *pSerialNum;
    char            aSerialNum[33] = {0, };

	UpdateData(TRUE);

    GetDlgItemText(IDC_SERIAL_NUMBER, cSerialNum);
	
    pSerialNum = LPSTR(LPCSTR(cSerialNum));
    memcpy(aSerialNum, pSerialNum, cSerialNum.GetLength());
	ATEC_SetSerial((unsigned char *)aSerialNum);

	UpdateData(FALSE);
}

void CATECTPDlg::OnBnClickedReset()
{
	CString str;
	int res;

	UpdateData(TRUE);

	m_bUSBOpen = FALSE;

	res = ATEC_Reset(0);

	BYTE ver[5];

	memset(ver, 0x00, sizeof(ver));

	res = ATEC_GetFirmwareVersion(&ver[0]);

	str.Format("ver %s\n", ver);

	OnMasData(str);

	CString title;
	title.Format("ATECTP version %c.%c%c%c", ver[0], ver[1], ver[2], ver[3]);
	SetWindowText(title);

	BYTE bRes[4];

	memset(bRes, 0x00, sizeof(bRes));

	WordToBig(res, &bRes[0]);

	str.Format("bRes %02x %02x %02x %02x\n", bRes[0], bRes[1], bRes[2], bRes[3]);
	OnMasData(str);

	BYTE bSerial[33];

	memset(bSerial, 0x00, sizeof(bSerial));

	res = ATEC_GetSerial(&bSerial[0]);

	memset(bRes, 0x00, sizeof(bRes));

	WordToBig(res, &bRes[0]);

	str.Format("%s\n", bSerial);
    ((CEdit *)GetDlgItem(IDC_SERIAL_NUMBER))->SetWindowText(str);

	OnMasData(str);

	m_bUSBOpen = TRUE;

	UpdateData(FALSE);
}


void CATECTPDlg::OnBnClickedCalibration()
{
	Calibration_Step(CALIB_STEP_OUT);
}


void CATECTPDlg::OnBnClickedFactoryInit()
{
	CString str;
	int res;

	UpdateData(TRUE);

	m_bUSBOpen = FALSE;

	res = ATEC_Reset(1);

	BYTE ver[5];

	memset(ver, 0x00, sizeof(ver));

	res = ATEC_GetFirmwareVersion(&ver[0]);

	str.Format("ver %s\n", ver);

	OnMasData(str);

	CString title;
	title.Format("ATECTP version %c.%c%c%c", ver[0], ver[1], ver[2], ver[3]);
	SetWindowText(title);

	BYTE bRes[4];

	memset(bRes, 0x00, sizeof(bRes));

	WordToBig(res, &bRes[0]);

	str.Format("bRes %02x %02x %02x %02x\n", bRes[0], bRes[1], bRes[2], bRes[3]);
	OnMasData(str);

	BYTE bSerial[33];

	memset(bSerial, 0x00, sizeof(bSerial));

	res = ATEC_GetSerial(&bSerial[0]);

	memset(bRes, 0x00, sizeof(bRes));

	WordToBig(res, &bRes[0]);

	str.Format("%s\n", bSerial);
    ((CEdit *)GetDlgItem(IDC_SERIAL_NUMBER))->SetWindowText(str);

	OnMasData(str);

	m_bUSBOpen = TRUE;

	UpdateData(FALSE);
}


void CATECTPDlg::OnBnClickedFwUpdate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CATECTPDlg::OnBnClickedFont()
{
    CString str = _T("BMP files (*.bmp)");
    CFileDialog dlg(TRUE, _T("*.bmp"), NULL, OFN_HIDEREADONLY, str);

    if (dlg.DoModal() == IDOK)
    {
        CString         path            = dlg.GetPathName();
        FILE            *fd             = fopen(path, "rb");
        int             width, height;
        unsigned char   *head = new unsigned char[54];
        int             lineSize;

        fread(head, 1, 54, fd);

        width = head[18] + (((int)head[19]) << 8) + (((int)head[20]) << 16) + (((int)head[21]) << 24);
        height = head[22] + (((int)head[23]) << 8) + (((int)head[24]) << 16) + (((int)head[25]) << 24);

        if (m_font != NULL) {
            delete(m_font);
            m_fontSize = 0;
        }

        lineSize   = (width / 8 + (width / 8) % 4);
        m_fontSize = lineSize * height;
        m_font = new unsigned char[m_fontSize];

        fseek(fd, 54, SEEK_SET);
        fseek(fd, 8, SEEK_CUR);
        fread(m_font, 1, m_fontSize, fd);

        delete(head);
        fclose(fd);
    }
}


void CATECTPDlg::OnBnClickedFontDown()
{
    ATEC_TPHBrandInfo(m_fontSize);
    ATEC_TPHBrandData(m_font, m_fontSize);

    delete(m_font);
    m_fontSize = 0;
}
