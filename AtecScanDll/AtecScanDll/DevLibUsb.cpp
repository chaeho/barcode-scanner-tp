// DevUsb.cpp : OMR USB Device Interface
//

#include "stdafx.h"
#include "lusb0_usb.h"
#include "DevLibUsb.h"
#include "Protocol.h"

#pragma warning(disable : 4996)

#define IMAGE_WIDTH_PIXEL       (832)
#define BRAND_CHUNK_SIZE        (256)

#define CMD_PKT_LENGTH          (64)
#define RSP_PKT_HEADER_LENGTH   (8)
#define RSP_PKT_STATUS_LENGTH   (16)
#define RSP_PKT_OMR_LENGTH      (64)
#define RSP_NON_DATA_PKT_LENGTH (64)



CDevLibUsb::CDevLibUsb(void)
{
	m_phUsb = NULL;
	m_bIsOpen = FALSE;
	m_bIsDebug = FALSE;
}

CDevLibUsb::~CDevLibUsb(void)
{
	TRACE("CDevLibUsb 소멸 \n");
}

void CDevLibUsb::SetDebugMode(BOOL bIsDebug)
{
	//m_bIsDebug = bIsDebug;
}

/***************************************************************************//**
 * @brief			Get Instance
 * @param [in]		None
 * @retval			CDevLibUsb* instance
 ******************************************************************************/
CDevLibUsb* CDevLibUsb::GetInstance(void)
{
	static CDevLibUsb *pObject = NULL;

	if (pObject == NULL)
	{
		pObject = new CDevLibUsb();
	}

	return pObject;
}

int CDevLibUsb::BigToWord(BYTE *pbSrcBig)
{
	int Res = 0;

	Res = pbSrcBig[0] << 24 | pbSrcBig[1] << 16 | pbSrcBig[2] << 8 | pbSrcBig[3];
	return Res;
}

int CDevLibUsb::WordToBig(int wSrc, BYTE *pbDestBig)
{
	BYTE *p;

	p = (BYTE*)&wSrc;

	pbDestBig[0] = *(p + 3);
	pbDestBig[1] = *(p + 2);
	pbDestBig[2] = *(p + 1);
	pbDestBig[3] = *p;

	return 4;
}

/***************************************************************************//**
 * @brief			Open USB Device
 * @param [in]		None
 * @retval			성공 > 0, 실패 <= 0
 ******************************************************************************/
int CDevLibUsb::OpenDevice(void)
{
	int nRet;
	struct usb_bus *bus;
	struct usb_device *dev;
	usb_dev_handle* hDev;

	usb_init();			/* initialize the library */
	usb_find_busses();	/* find all busses */
	usb_find_devices();	/* find all connected devices */

	m_bIsOpen = FALSE;

	for (bus = usb_get_busses(); bus; bus = bus->next)
	{
		for (dev = bus->devices; dev; dev = dev->next)
		{
			if ((dev->descriptor.idVendor == MY_VID) && (dev->descriptor.idProduct == MY_PID))
			{
				hDev = usb_open(dev);
				if (hDev == NULL)
				{
					continue;
				}

				nRet = usb_set_configuration(hDev, MY_CONFIG);
				if (nRet < 0)
				{
					//TRACE(_T("error setting config #%d: %s\n"), MY_CONFIG, usb_strerror());
					LogOut("error setting config #%d: %s\n", MY_CONFIG, usb_strerror());
					usb_close(hDev);
					return -1;
				}
				else
				{
					LogOut("success: set configuration #%d\n", MY_CONFIG);
				}

				nRet = usb_claim_interface(hDev, 0);
				if (nRet < 0)
				{
					LogOut("error claiming interface #%d:\n%s\n", MY_INTF, usb_strerror());
					usb_close(hDev);
					return -2;
				}
				else
				{
					LogOut("success: claim_interface #%d\n", MY_INTF);
				}

				m_phUsb = hDev;
				m_bIsOpen = TRUE;

				return 1;
			}
		}
	}

	LogOut("error: not open #%d\n", MY_INTF);
	return -9;
}

/***************************************************************************//**
 * @brief			Close USB Device
 * @param [in]		None
 * @retval			성공 > 0, 실패 <= 0
 ******************************************************************************/
int CDevLibUsb::CloseDevice(void)
{
    m_bIsOpen = FALSE;

	if (m_phUsb == NULL) 
	{ 
        // if device is close, then error
		return -1;
	}

	usb_close(m_phUsb);
	m_phUsb = NULL;

	return 1;
}

int CDevLibUsb::Reset(void)
{
	int         nResult;
	BYTE        bTemp[4] = {0,};

	int         i, nRet;
	SYSTEMTIME  stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Reset] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x00;
    pktBuf[1] = 0x00;

	HexaDump("Reset_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Reset_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Reset] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Get_Fimware_Version(BYTE *data)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Get_Fimware_Version] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x00;
    pktBuf[1] = 0x01;

	HexaDump("Get_Fimware_Version_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Get_Fimware_Version_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			HexaDump("Get_Fimware_Version_RecvData", &bTemp[0], 4);

			memcpy(data, (BYTE *)&pktBuf[RSP_PKT_HEADER_LENGTH], 4);
			
			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Get_Fimware_Version] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Get_Serial_Num(BYTE *data)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Get_Serial_Num] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

	pktBuf[0] = 0x00;
	pktBuf[1] = 0x02;

	HexaDump("Get_Serial_Num_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Get_Serial_Num_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			memcpy(data, &pktBuf[RSP_PKT_HEADER_LENGTH], 32);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Get_Serial_Num] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Set_Serial_Num(BYTE *Send_Data, int Len)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Set_Serial_Num] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

	pktBuf[0] = 0x00;
	pktBuf[1] = 0x03;		

	memcpy(&pktBuf[2], Send_Data, Len);

	HexaDump("Set_Serial_Num_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO * 3);
		if (nRet > 0)
		{
			HexaDump("Set_Serial_Num_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Set_Serial_Num] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Check_Status(BYTE *data)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Check_Status] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x00;
    pktBuf[1] = 0x04;

	HexaDump("Check_Status_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Check_Status_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			memcpy(data, &pktBuf[RSP_PKT_HEADER_LENGTH], RSP_PKT_STATUS_LENGTH);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Check_Status] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Get_Param(BYTE *data)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Get_Param] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x01;
    pktBuf[1] = 0x00;

	HexaDump("Get_Param_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error

		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, (char *)pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Get_Param_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			memcpy(data, &pktBuf[RSP_PKT_HEADER_LENGTH], 32);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Get_Param] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Set_Param(BYTE *Send_Data, int Len)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Set_Param] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x01;
    pktBuf[1] = 0x01;

	memcpy(&pktBuf[2], Send_Data, Len);

	HexaDump("Set_Param_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Set_Param_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Set_Param] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Motor_On(BYTE identifier, BYTE dir)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Motor_On] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = { 0, };

	pktBuf[0] = 0x02;
	pktBuf[1] = 0x02;
	pktBuf[2] = identifier;
	pktBuf[3] = dir;

	HexaDump("Motor_On_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Motor_On_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Motor_On] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Motor_Off(BYTE identifier)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Motor_Off] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = { 0, };

	pktBuf[0] = 0x02;
	pktBuf[1] = 0x03;
	pktBuf[2] = identifier;

	HexaDump("Motor_Off_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0) {
			HexaDump("Motor_Off_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Motor_Off] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Led_Control(BYTE is_on)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Led_Control] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

	pktBuf[0] = 0x02;
	pktBuf[1] = 0x04;
	pktBuf[2] = is_on;

	HexaDump("Led_Control_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error

		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0) {
			HexaDump("Led_Control_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Led_Control] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Scan_Start(unsigned int IsOMR)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Scan_Start] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x03;
    pktBuf[1] = 0x00;

    if (IsOMR == TRUE)
    {
        pktBuf[2] = 1;
    }

	HexaDump("Scan_Start_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0) {
			HexaDump("Scan_Start_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Scan_Start] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Scan_Stop(void)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Scan_Stop] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x03;
    pktBuf[1] = 0x01;

	HexaDump("Scan_Stop_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Scan_Stop_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Scan_Stop] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Get_Image(int Line, unsigned char *FrontRawBuf, unsigned char *RearRawBuf)
{
	int             nResult;
	BYTE            bTemp[4] = {0,};
	int             nRet, nRetry;
	int             img_size;
	int             Recv_size;
	int             nCnt;
    unsigned char   *imgBuf;

	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	img_size	= Line * IMAGE_WIDTH_PIXEL;

	int nSize = img_size * MAX_IMG_CNT;

    Recv_size = (Line * IMAGE_WIDTH_PIXEL * MAX_IMG_CNT) + RSP_PKT_OMR_LENGTH + RSP_PKT_HEADER_LENGTH;

	LogOut("ReadScan_2(), Recv_size(%lu), m_nWidth(%d), m_nHeight(%d), img_size(%lu) \n", Recv_size, IMAGE_WIDTH_PIXEL, Line, img_size);

    imgBuf = new unsigned char[Recv_size];

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Get_Image] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

	pktBuf[0] = 0x03;
	pktBuf[1] = 0x02;

	HexaDump("Get_Image_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	nRetry	= 0;
	nCnt	= 0;
	do 
	{
        nRet = usb_bulk_read(m_phUsb, EP_IN, (char *)(imgBuf + nCnt), Recv_size - nCnt, MAX_USB_TMO);
		if (nRet < 0)
		{
			if (++nRetry > 3)
			{
				bTemp[0] = 0x04;

				nResult = BigToWord(&bTemp[0]);

				return nResult;
			}
			continue;
		}

		nCnt += nRet;
	} while (nCnt < Recv_size);

    /* mirroring */
    nCnt = 0;
    for (int h = Line - 1; h >= 0; h--)
    {
        for (int v = 0; v < IMAGE_WIDTH_PIXEL; v++)
        {
            //FrontRawBuf[h * IMAGE_WIDTH_PIXEL + v] = (127 < (imgBuf[RSP_PKT_OMR_LENGTH + RSP_PKT_HEADER_LENGTH + (nCnt * MAX_IMG_CNT)])) ? 255 : 0;
            //RearRawBuf[h * IMAGE_WIDTH_PIXEL + v] = (127 < (imgBuf[RSP_PKT_OMR_LENGTH + RSP_PKT_HEADER_LENGTH + 1 + (nCnt * MAX_IMG_CNT)])) ? 255 : 0;
            FrontRawBuf[h * IMAGE_WIDTH_PIXEL + v] = imgBuf[RSP_PKT_OMR_LENGTH + RSP_PKT_HEADER_LENGTH + (nCnt * MAX_IMG_CNT)];
            RearRawBuf[h * IMAGE_WIDTH_PIXEL + v] = imgBuf[RSP_PKT_OMR_LENGTH + RSP_PKT_HEADER_LENGTH + 1 + (nCnt * MAX_IMG_CNT)];

            nCnt++;
        }
    }

	nResult = BigToWord((BYTE *)&imgBuf[2]);

	LogOut("[nResult] - %d\n", nResult);

	memset(bTemp, 0x00, sizeof(bTemp));

	WordToBig(nResult, &bTemp[0]);

    delete(imgBuf);

	return nResult;
}

int CDevLibUsb::Get_Axis(unsigned char *data)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Get_Axis] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x03;
    pktBuf[1] = 0x03;

	HexaDump("Get_Axis_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}
	
    int Receive_size = RSP_PKT_HEADER_LENGTH + RSP_PKT_OMR_LENGTH;

	BYTE* tempbuf = new BYTE[Receive_size];
	memset(tempbuf, 0, Receive_size);

    int nRetry = 0;
    int nCnt = 0;
	
	do 
	{
		nRet = usb_bulk_read(m_phUsb, EP_IN, (char *)tempbuf, Receive_size, MAX_USB_TMO);
		if (nRet < 0)
		{
			if (++nRetry > 3)
			{
				bTemp[0] = 0x04;				                

                nResult = BigToWord(&bTemp[0]);

				return nResult;
			}
			continue;
		}
		nCnt += nRet;
	} while (nCnt < Receive_size);

    HexaDump("Get_Axis_RecvData", tempbuf, nRet);

	memcpy(data, &tempbuf[RSP_PKT_HEADER_LENGTH], RSP_PKT_OMR_LENGTH);

	nResult = BigToWord(&tempbuf[2]);

	LogOut("[nResult] - %d\n", nResult);

	memset(bTemp, 0x00, sizeof(bTemp));

	WordToBig(nResult, &bTemp[0]);

    delete(tempbuf);

	return nResult;
}

int CDevLibUsb::Eject(BYTE Type)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE)
	{
		// if device is close, then error
		LogOut("[Eject] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x04;
    pktBuf[1] = 0x00;
    pktBuf[2] = Type;

	HexaDump("Eject_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Eject_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Eject] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Reject(void)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE)
	{
		// if device is close, then error
		LogOut("[Reject] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };
    pktBuf[0] = 0x04;
    pktBuf[1] = 0x01;

	HexaDump("Reject_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Reject_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Reject] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}


int CDevLibUsb::TPH_BrandInfo(unsigned short Len)
{
	int         i, nRet;
	SYSTEMTIME  stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[TPH_Print] - m_bIsOpen = FALSE...\n");
        return 1;
	}

    /* command code field */
    char pktBuf[CMD_PKT_LENGTH];
    pktBuf[0] = 0x05;
    pktBuf[1] = 0x00;

    /* image size */
    pktBuf[2] = (Len >> 8) & 0xFF;
    pktBuf[3] = Len & 0xFF;

    /* image field */
	HexaDump("TPH_BrandInfo_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);

    nRet = usb_bulk_write(m_phUsb, EP_OUT, (char *)pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
    if (nRet < 0)
    {
        return 2;
    }

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("TPH_BrandInfo_RecvData", (BYTE *)pktBuf, nRet);

			LogOut("[nResult] - 0\n");

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[TPH_BrandInfo] - Command Recv() Error ...\n");
        return -1;
	}

	return 0;
}

int CDevLibUsb::TPH_BrandData(unsigned char *pImage, int Len)
{
    BYTE    Packet[BRAND_CHUNK_SIZE + 2];
    int     TxLen, TxPtr;
    int     nRet;
    int     i, nCount = 0;

    if (m_bIsOpen == FALSE)
    {
        // if device is close, then error
        return 1;
    }

    /* command code field */
    Packet[0] = 0x05;
    Packet[1] = 0x01;

    TxLen = Len;
    TxPtr = 0;

	LogOut("[%s], length(%d)...", __FUNCTION__, Len );

    if (TxLen == 0)
    {
        return -1;
    }

    do
    {
		char Temp[BRAND_CHUNK_SIZE] = { 0, };

        if (TxLen >= BRAND_CHUNK_SIZE)
        {
			sprintf(Temp, "#1_Send_Branding Data_%02d", ++nCount);
			HexaDump(Temp, (BYTE *)&pImage[TxPtr], BRAND_CHUNK_SIZE);

            memcpy(&Packet[2], (char *)&pImage[TxPtr], BRAND_CHUNK_SIZE);
            nRet = usb_bulk_write(m_phUsb, EP_OUT, (char *)Packet, BRAND_CHUNK_SIZE + 2, MAX_USB_TMO);
            if (nRet < 0)
            {
                return nRet;
            }

            for (i = 0; i < 3; i++)
            {
                ::ZeroMemory(Packet, sizeof(Packet));
                nRet = usb_bulk_read(m_phUsb, EP_IN, (char *)Packet, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
                if (nRet > 0)
                {
					sprintf(Temp, "#1_Recv_Branding Data_, ret = %02d", nRet);
					HexaDump(Temp, (BYTE *)Packet, nRet);
					break;
                }
                else
                {
                    return -1;
                }

                ::Sleep(10);
            }

            TxLen -= BRAND_CHUNK_SIZE;
            TxPtr += BRAND_CHUNK_SIZE;
        }
        else
        {
			sprintf(Temp, "#2_Send_Branding Data_%02d", ++nCount);
			HexaDump(Temp, (BYTE *)&pImage[TxPtr], BRAND_CHUNK_SIZE);
			
			memcpy(&Packet[2], (char *)&pImage[TxPtr], BRAND_CHUNK_SIZE);
            nRet = usb_bulk_write(m_phUsb, EP_OUT, (char *)Packet, TxLen, MAX_USB_TMO);
            if (nRet < 0)
            {
                return nRet;
            }

            for (i = 0; i < 3; i++)
            {
                ::ZeroMemory(Packet, sizeof(Packet));
                nRet = usb_bulk_read(m_phUsb, EP_IN, (char *)Packet, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
                if (nRet > 0)
                {
					sprintf(Temp, "#2_Recv_Branding Data_, ret = %02d", nRet);
					HexaDump(Temp, (BYTE *)Packet, nRet);
					break;
                }
                else
                {
                    return -1;
                }

                ::Sleep(10);
            }

            TxLen = 0;
        }
    } while (TxLen > 0);

    return 0;
}

int CDevLibUsb::TPH_Branding(BYTE dir, unsigned short PositionY)
{
	BYTE    aPacket[100];
	int     nRet;
	int     i;

	if (m_bIsOpen == FALSE)
	{
		// if device is close, then error
		return 1;
	}

	/* command code field */
	aPacket[0] = 0x05;
	aPacket[1] = 0x02;
	
	// dir
	aPacket[2] = dir & 0xFF;

	/* vertical position field */
	aPacket[3] = (PositionY >> 8) & 0xFF;
	aPacket[4] = PositionY & 0xFF;

	HexaDump("TPH_Branding_SendData", aPacket, CMD_PKT_LENGTH);

	nRet = usb_bulk_write(m_phUsb, EP_OUT, (char*)aPacket, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		return nRet;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(aPacket, sizeof(aPacket));
		nRet = usb_bulk_read(m_phUsb, EP_IN, (char*)aPacket, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("TPH_Branding_RecvData", aPacket, nRet);

			LogOut("[nResult] - 0\n");

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[%s] - Command Recv() Error ...\n", __FUNCTION__);

		return 4;
	}

	return 0;
}

int CDevLibUsb::Calibration(unsigned char step)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Calibration] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x06;
    pktBuf[1] = 0x00;
	pktBuf[2] = step;

	HexaDump("Calibration_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Calibration_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Calibration] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Firmware_Update(BYTE* Send_Data, int Len)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Firmware_Update] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = 0x0f;
    pktBuf[1] = 0x0f;
	memcpy(&pktBuf[2], Send_Data, Len);

	HexaDump("Firmware_Update_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, (char *)pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, (char *)pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Firmware_Update_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Firmware_Update] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

int CDevLibUsb::Factory_Init(void)
{
	int nResult = 0;
	BYTE bTemp[4] = {0,};

	int i, nRet;
	SYSTEMTIME stCurrent;

	GetLocalTime(&stCurrent);

	if (m_bIsOpen == FALSE) 
	{
		// if device is close, then error
		LogOut("[Factory_Init] - m_bIsOpen = FALSE...\n");
		bTemp[0] = 0x01;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

    char pktBuf[CMD_PKT_LENGTH] = {0, };

    pktBuf[0] = (char)0xff;
    pktBuf[1] = (char)0xff;

	HexaDump("Factory_Init_SendData", (BYTE *)pktBuf, CMD_PKT_LENGTH);
	nRet = usb_bulk_write(m_phUsb, EP_OUT, (char *)pktBuf, CMD_PKT_LENGTH, MAX_USB_TMO);
	if (nRet < 0)
	{
		// Error
		bTemp[0] = 0x02;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	// recv
	for (i = 0; i < 3; i++)
	{
		::ZeroMemory(pktBuf, sizeof(pktBuf));
		nRet = usb_bulk_read(m_phUsb, EP_IN, pktBuf, RSP_NON_DATA_PKT_LENGTH, MAX_USB_TMO);
		if (nRet > 0)
		{
			HexaDump("Factory_Init_RecvData", (BYTE *)pktBuf, nRet);

			nResult = BigToWord((BYTE *)&pktBuf[2]);

			LogOut("[nResult] - %d\n", nResult);

			memset(bTemp, 0x00, sizeof(bTemp));

			WordToBig(nResult, &bTemp[0]);

			break;
		}
		::Sleep(10);
	}

	if (nRet <= 0)
	{
		LogOut("[Factory_Init] - Command Recv() Error ...\n");
		bTemp[0] = 0x04;

		nResult = BigToWord(&bTemp[0]);

		return nResult;
	}

	return nResult;
}

static char usrMsg[1024 * 8];
SYSTEMTIME lt;

int CDevLibUsb::LogOut(const char *fmt, ...)
{
	FILE	*fd;
	va_list ap;
	int ltime;
	char name[64], path[256];

	if (m_bIsDebug == TRUE)
	{
		va_start(ap, fmt);
		vsprintf(usrMsg, fmt, ap);
		va_end(ap);

		memset(name, 0, sizeof(name));
		memset(path, 0, sizeof(path));

		GetLocalTime(&lt);
		strcpy(path, "c:\\ATEC T&\\");
		sprintf(name, "OMR_%04d%02d%02d.log", lt.wYear, lt.wMonth, lt.wDay);
		strcat(path, name);

		if ((fd = fopen(path, "a+")) == NULL)
		{
			printf("can't open %s\n", path);
			return -1;
		}

		ltime = lt.wHour * 10000 + lt.wMinute * 100 + lt.wSecond;
		fprintf(fd, "[%02d:%02d:%02d] %s", lt.wHour, lt.wMinute, lt.wSecond, usrMsg);
		fclose(fd);
	}

	return 0;
}

int CDevLibUsb::trace(const char *fmt, ...)
{
	FILE	*fd;
	va_list ap;
	char name[64], path[256];

	va_start(ap, fmt);
	vsprintf(usrMsg, fmt, ap);
	va_end(ap);

	memset(name, 0, sizeof(name));
	memset(path, 0, sizeof(path));

	GetLocalTime(&lt);

	strcpy(path, "c:\\ATEC T&\\");
	sprintf(name, "OMR_%04d%02d%02d.log", lt.wYear, lt.wMonth, lt.wDay);
	strcat(path, name);

	if ((fd = fopen(path, "a+")) == NULL)
	{
		printf("can't open %s\n", path);
		return -1;
	}
	fprintf(fd, "%s", usrMsg);
	fclose(fd);

	return 0;
}

void CDevLibUsb::HexaDump(char *pTitle, BYTE *data, int nLen)
{
	int i, off;
	char szTemp[256], *cp = szTemp;
	SYSTEMTIME lt;

	if (m_bIsDebug == TRUE)
	{
		off = 0;

		GetLocalTime(&lt);

		sprintf(szTemp, "[%02d:%02d:%02d] (%s) HexDump : Len = [%d] ", lt.wHour, lt.wMinute, lt.wSecond, pTitle, nLen);
		trace("%s", szTemp);

		for (i = 0; i < nLen; i++)
		{
			if (((i % 16) == 0) && i)
			{
				//cp += sprintf(cp, "\n");
				trace(">%s", szTemp);
				cp = szTemp;
			}
			if ((i % 4) == 0)
			{
				cp += sprintf(cp, " ");
			}
			cp += sprintf(cp, "[%02X]", data[i] & 0xFF);
		}
		trace(">%s", szTemp);

		sprintf(szTemp, "--------------------------------------------------------------------\n");
		trace(">%s", szTemp);
	}
}
