#include "stdafx.h"
#include "BarcodeRead.h"

#include <string>
#include <iostream>
#include "zbar.h"

#pragma warning(disable : 4996)

#define STR(s)						#s

#define WIDTHBYTES(bits)			(((bits) + 31) / 32 * 4)

using namespace std;
using namespace zbar;

CBarcodeRead::CBarcodeRead(void)
{

}

CBarcodeRead::~CBarcodeRead(void)
{

}

/***************************************************************************//**
 * @brief			Get Instance
 * @param [in]		None
 * @retval			CBarcodeRead* instance
 ******************************************************************************/
CBarcodeRead* CBarcodeRead::GetInstance(void)
{
	static CBarcodeRead *pObject = NULL;

	if(pObject == NULL)
	{
		pObject = new CBarcodeRead();
	}

	return pObject;
}

void CBarcodeRead::SetDebugMode(BOOL bIsDebug)
{
	m_bIsDebug = bIsDebug;
}

int CBarcodeRead::BarcodeRead(unsigned char *rawfile, int Width, int Height, char* Read_Data, unsigned short *posY)
{
	int				barcode_type = 0;
	unsigned short	SmalY		= 0xFFFF;
	unsigned short	CurrY;
	int				i;

	string			type_name("");
	string			barcode("");
	CString			temp_data, temp_data2;

	// create a reader
	ImageScanner    scanner;

	// configure the reader
	scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);

    Image image(Width, Height, "Y800", rawfile, Width * Height);

	// scan the image for barcodes
	int n = scanner.scan(image);

	// extract results
	for (Image::SymbolIterator symbol = image.symbol_begin();
		 symbol != image.symbol_end();
		 ++symbol)
	{
		// do something useful with results
		type_name = symbol->get_type_name();
		barcode   = symbol->get_data();

		temp_data  = type_name.c_str();
		temp_data2 = barcode.c_str();

		if (m_bIsDebug == TRUE)
		{
			LogOut("%s", symbol->get_data());
			LogOut("%d", symbol->get_type());
		}

		strncpy(Read_Data, barcode.c_str(), barcode.length());
		barcode_type = (int)symbol->get_type();

		int nn = symbol->get_location_size();

		for (i = 0; i < nn; i++)
		{
			CurrY = symbol->get_location_y(i);

			if (SmalY > CurrY)
			{
				SmalY = CurrY;
			}

			if (m_bIsDebug == TRUE)
			{
				LogOut("x:%d y:%d", symbol->get_location_x(i), CurrY);
			}
		}
	}

	/* return the start position y */
	*posY = SmalY;

	// clean up
	//image.set_data(NULL, 0);

	return barcode_type;
}

void CBarcodeRead::FileRead(char* FilePath, int Width, int Height, BYTE* mem, int TypeSize)
{
	FILE* fp;
	int errorno =0;
	int FileLen = Width * Height;

	if ((errorno = fopen_s(&fp, FilePath, "rb")))
	{
		return;
	}

	fseek(fp,0L,SEEK_END);
	
	long len = ftell(fp);
	fseek(fp, len - FileLen * TypeSize, SEEK_SET);

	int ReadCount = fread(mem, TypeSize, FileLen, fp);
	if (ReadCount < FileLen)
	{
		TRACE("Read Count : %d\n", ReadCount);
		return ;
	}

	fclose(fp);
}

BYTE CBarcodeRead::BinToBcd(BYTE Bin)
{
	return (((Bin / 10) << 4) + (Bin % 10));
}

void CBarcodeRead::GetTimeBCD(BYTE* BCDTime)
{
	CTime tm;
	BYTE time_sync[7];

	tm = CTime::GetCurrentTime();
	time_sync[0] = (BYTE)(tm.GetYear() / 100);
	time_sync[1] = (BYTE)(tm.GetYear() % 100);
	time_sync[2] = (BYTE)tm.GetMonth();
	time_sync[3] = (BYTE)tm.GetDay();
	time_sync[4] = (BYTE)tm.GetHour();
	time_sync[5] = (BYTE)tm.GetMinute();
	time_sync[6] = (BYTE)tm.GetSecond();

	for (int i = 0; i < 7; i++)
	{
		BCDTime[i] = BinToBcd(time_sync[i]);
	}
}

BYTE CBarcodeRead::Clamp_Byte(BYTE data)
{
	return (data > 255) ? 255 : ((data < 0) ? 0 : data);
}

void CBarcodeRead::RawToBmp(int Width, int Height, unsigned char *RawData, char *FileName)
{
	BITMAPFILEHEADER fh;
	BITMAPINFOHEADER ih;
	RGBQUAD rgb[256];

	memset(&fh, 0, sizeof(BITMAPFILEHEADER));
	memset(&ih, 0, sizeof(BITMAPINFOHEADER));
	memset(&rgb, 0, sizeof(RGBQUAD) * 256);

	fh.bfOffBits = 1078; // RGBQUAD + InfoHeader + FileHeader only 8bit mode if 24bit == 54; 40+ 14; 

	fh.bfSize = Width * Height + sizeof(RGBQUAD) * 256 + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	fh.bfType = 19778;

	ih.biBitCount = 8;
	ih.biHeight = Height;
	ih.biPlanes = 1;
	ih.biSize = 40;
    ih.biSizeImage = 0;//Width * Height;
	ih.biWidth = Width;
	ih.biXPelsPerMeter = 0;
	ih.biYPelsPerMeter = 0;

	for (int i = 0; i < 256; i++)
	{
		rgb[i].rgbBlue = i;
		rgb[i].rgbGreen = i;
		rgb[i].rgbRed = i;
		rgb[i].rgbReserved = 0;
	}

	int rwsize = WIDTHBYTES(ih.biBitCount*ih.biWidth);

	FILE* fp;
    fopen_s(&fp, FileName, "wb");
	fwrite(&fh, sizeof(BITMAPFILEHEADER), 1, fp);
	fwrite(&ih, sizeof(BITMAPINFOHEADER), 1, fp);
	fwrite(rgb, sizeof(RGBQUAD), 256, fp);
	fwrite(RawData, 1, Width * Height, fp);
	fclose(fp);
}

static char usrMsg[1024*8];

int CBarcodeRead::LogOut(const char *fmt, ...)
{
	FILE	*fd;
	va_list ap;
	char name[64], path[256];
	BYTE EDate[7] = {0,};

	va_start(ap, fmt);
	vsprintf(usrMsg, fmt, ap);
	va_end(ap);

	memset(name,0,sizeof(name));
	memset(path, 0, sizeof(path));

	GetTimeBCD(&EDate[0]);

	strcpy(path, "c:\\temp\\");

	sprintf(name,"BAR_%02x%02x%02x%02x.log", EDate[0], EDate[1], EDate[2], EDate[3]);
	strcat(path, name);

	if ((fd = fopen(path, "a+")) == NULL)
	{
		printf("can't open %s\n", path);
		return -1;
	}
		
	fprintf(fd,"[%02x:%02x:%02x] %s\n", EDate[4], EDate[5], EDate[6], usrMsg);
	fclose(fd);

	return 0;
}