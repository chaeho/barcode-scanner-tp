// mybmp.h : mybmp.cpp의 기본 헤더 파일입니다.
//

#pragma once

#define TOP_DIR					1
#define BOTTOM_DIR				2

#define	MAX_MY_BUFFER			512
#define WIDTHBYTES(bits, w)		((w * (bits / 8) + 3) & ~3)


#define	THRESHHOLD	140
#if 0
#define	PIX_IN_MM	(33/10)
#define	RECT_Y_MIN	3
#define	RECT_Y_MAX	10
#define	RECT_X_MIN	10
#define	RECT_X_MAX  25

#else
#define	PIX_IN_MM	7
#define	RECT_Y_MIN	6
#define	RECT_Y_MAX	20
#define	RECT_X_MIN	20
#define	RECT_X_MAX  45

#endif

#pragma pack(1)

typedef struct _POINT 
{
	short x;
	short y;
} point_t;

typedef struct _RECT 
{
	point_t top;
	point_t bottom;
} rect_t;

#define	SZ_RECT	300
typedef struct _horse_tag 
{
	rect_t	ra[SZ_RECT];
	short	nRect;
	char	dir;			///< ORIGIN(1,2,3,4)--right/top, left/bottom, left/top, right/bottom
	char	type;			///< 구매권(A), 구매표(B), 기타('C')
	int		width;
	int		height;
	int		byte_per_row;
	char	rbin[12];
	unsigned char *img;
} horse_tag_t;

#pragma pack()


class CMyBitmap
{
public:
	CMyBitmap(void);
	virtual ~CMyBitmap(void);

	static CMyBitmap* GetInstance(void);

public:

	int				m_nFrameWidth;
	int				m_nFrameHeight;
	int				m_nBytesPerRow;

	int				sum_xa[200 * 8], sum_xa1[200 * 8];
	int				sum_ya[200 * 4], sum_ya1[200*4];
	long			fcur;

	char			m_szFrontRawFile[MAX_MY_BUFFER];
	char			m_szRearRawFile[MAX_MY_BUFFER];

	char			m_szFrontCropFile[MAX_MY_BUFFER];
	char			m_szRearCropFile[MAX_MY_BUFFER];

	char			m_szRawFile[MAX_MY_BUFFER];
	char			m_szCropFile[MAX_MY_BUFFER];

	horse_tag_t		m_tBetSlip;
	rect_t			ra[100];

	BOOL			m_bDebug;

	BYTE*	m_pszImgData;
	BYTE*	m_pImageBuf;

public:
	void SetDebugMode(BOOL IsTrue);

	void SetFilePath(char *pFileName);

	int SaveRawImageFile(char *pFileName, BYTE *pRawData, int nDataLen);
	int WriteImgFile(int nDir, int xsize, int ysize, BYTE *pImg, int nBytePerPixel);
	int ReadImgFile(char *pFileName);

	void postProc(void);
	void calcSumImg(unsigned int *sum,int x1, int y1, int x2, int y2, char mode);
	void find_1_image(unsigned int *sum_a, int st, int ed, unsigned short *seg, short *lenseg);
	int find_rect(void);
	void checkOrigin(void);
	int checkSlip(void);
	int ImageProcessing(int nDir, BYTE *retBuf);
	//int LogOut(const char *fmt, ...);
	//int trace(const char *fmt, ...);
	//void HexaDump(char *pTitle, BYTE *data, int nLen);
};



