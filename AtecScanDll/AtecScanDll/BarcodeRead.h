class CBarcodeRead
{
public:
	CBarcodeRead(void);
	virtual ~CBarcodeRead(void);

	static CBarcodeRead* GetInstance(void);

public:
	BOOL				m_bIsDebug;

	BYTE BinToBcd(BYTE Bin);
	void GetTimeBCD(BYTE* BCDTime);
	void FileRead(char* FilePath, int Width, int Height, BYTE* mem, int TypeSize);
	int BarcodeRead(unsigned char *rawfile, int Width, int Height, char* Read_Data, unsigned short *posY);
	void RawToBmp(int Width, int Height, unsigned char *RawData, char *FileName);
	int LogOut(const char *fmt, ...);
	BYTE Clamp_Byte(BYTE data);

	void SetDebugMode(BOOL bIsDebug);
};
