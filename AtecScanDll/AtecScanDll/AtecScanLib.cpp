// AtecScanLib.cpp : 해당 DLL의 초기화 루틴을 정의합니다.
//

#include "stdafx.h"
#include "AtecScanLib.h"
#include "DevLibUsb.h"
#include "BarcodeRead.h"
#include "MyBmp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern "C" __declspec(dllexport) int ATEC_DeviceOpen(BOOL IsDebugMode)
{
	int res = 0;

	if (CDevLibUsb::GetInstance()->OpenDevice() < 0)	// fail
	{
		res = -1;
	}
	else
	{
		CDevLibUsb::GetInstance()->SetDebugMode(IsDebugMode);
		CBarcodeRead::GetInstance()->SetDebugMode(IsDebugMode);
	}

	return res;
}

extern "C" __declspec(dllexport) int ATEC_DeviceClose()
{
	int res = 0;

	if (!CDevLibUsb::GetInstance()->CloseDevice())
    {
		res = -1;
	}
	return res;
}

extern "C" __declspec(dllexport) int ATEC_IsDevice()
{
	int res = 0;

	if (CDevLibUsb::GetInstance()->m_bIsOpen == FALSE)
    {
		res = -1;
	}
	return res;
}

extern "C" __declspec(dllexport) int ATEC_SaveBMP(int Width, int Height, unsigned char *RawData, char *FileName)
{
	CBarcodeRead::GetInstance()->RawToBmp(Width, Height, RawData, FileName);

    return 0;
}

extern "C" __declspec(dllexport) int ATEC_Inject(unsigned char dir)
{
	return CDevLibUsb::GetInstance()->Eject(dir);
}

extern "C" __declspec(dllexport) int ATEC_Reject()
{
	return CDevLibUsb::GetInstance()->Reject();
}

extern "C" __declspec(dllexport) int ATEC_LEDOn()
{
	return CDevLibUsb::GetInstance()->Led_Control(1);
}

extern "C" __declspec(dllexport) int ATEC_LEDOff()
{
	return CDevLibUsb::GetInstance()->Led_Control(0);
}

extern "C" __declspec(dllexport) int ATEC_GetStatus(unsigned char *status)
{
	return CDevLibUsb::GetInstance()->Check_Status(status);
}

extern "C" __declspec(dllexport) int ATEC_SetParam(unsigned char *data, int data_len)
{
	return CDevLibUsb::GetInstance()->Set_Param(data, data_len);
}

extern "C" __declspec(dllexport) int ATEC_GetParam(unsigned char *data)
{
	return CDevLibUsb::GetInstance()->Get_Param(data);
}

extern "C" __declspec(dllexport) int ATEC_GetFirmwareVersion(unsigned char *ver)
{
	return CDevLibUsb::GetInstance()->Get_Fimware_Version(ver);
}

extern "C" __declspec(dllexport) int ATEC_ScannerOn(unsigned int IsOMR)
{
	return  CDevLibUsb::GetInstance()->Scan_Start(IsOMR);
}

extern "C" __declspec(dllexport) int ATEC_ScannerOff()
{
	return CDevLibUsb::GetInstance()->Scan_Stop();
}

extern "C" __declspec(dllexport) int ATEC_ScanBuf(int Scan_Len, unsigned char *FrontRawBuf, unsigned char *RearRawBuf)
{
	return CDevLibUsb::GetInstance()->Get_Image(Scan_Len, FrontRawBuf, RearRawBuf);
}

extern "C" __declspec(dllexport) int ATEC_MotorStart(char identifier, char dir)
{
	return CDevLibUsb::GetInstance()->Motor_On(identifier, dir);
}

extern "C" __declspec(dllexport) int ATEC_MotorStop(char identifier)
{
	return CDevLibUsb::GetInstance()->Motor_Off(identifier);
}

extern "C" __declspec(dllexport) int ATEC_Reset(int mode)
{
	int res;

	if (mode == 0)
	{
		res = CDevLibUsb::GetInstance()->Reset();
	}
	else
	{
		res = CDevLibUsb::GetInstance()->Factory_Init();
	}

	return res;
}

extern "C" __declspec(dllexport) int ATEC_TPHBrandInfo(int TPH_Data_Len)
{
	return CDevLibUsb::GetInstance()->TPH_BrandInfo(TPH_Data_Len);
}

extern "C" __declspec(dllexport) int ATEC_TPHBrandData(unsigned char *TPH_Data, int Len)
{
    return CDevLibUsb::GetInstance()->TPH_BrandData(TPH_Data, Len);
}

extern "C" __declspec(dllexport) int ATEC_TPHBranding(BYTE dir, unsigned short PositionY)
{
	return CDevLibUsb::GetInstance()->TPH_Branding(dir, PositionY);
}

extern "C" __declspec(dllexport) int ATEC_GetSerial(unsigned char *serial)
{
	return CDevLibUsb::GetInstance()->Get_Serial_Num(serial);
}

extern "C" __declspec(dllexport) int ATEC_SetSerial(unsigned char *serial)
{
	return CDevLibUsb::GetInstance()->Set_Serial_Num(serial, DEVICE_SN_LENGTH);
}

extern "C" __declspec(dllexport) int ATEC_Calibration(unsigned char step)
{
	return CDevLibUsb::GetInstance()->Calibration(step);
}

extern "C" __declspec(dllexport) int ATEC_Barcode(unsigned char *rawfile, int Width, int Height, char* barcode_data, unsigned short *posY)
{
	return CBarcodeRead::GetInstance()->BarcodeRead(rawfile, Width, Height, barcode_data, posY);
}

extern "C" __declspec(dllexport) int ATEC_SendOMR(unsigned char *pBitMap)
{
    return CDevLibUsb::GetInstance()->Get_Axis(pBitMap);
}