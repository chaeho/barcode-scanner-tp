// mybmp.h : mybmp.cpp의 기본 헤더 파일입니다.
//

#pragma once


#pragma pack(1)

typedef struct 
{
	BYTE	szCmd[2];
	BYTE	byResult;
	BYTE	byStatus;
	WORD	wDevStatus;			///< 
	DWORD	dwError;			///< 0x00 : 에러없음, 
	BYTE	byTicket;			///< 0x01 : 구매표, 0x02 : 구매권
	//WORD	wImgLength;			///< 이미지 길이
	BYTE	byImgLength[2];		///< 이미지 길이
} RECV_SCAN_START_T, *PRECV_SCAN_START_T;

typedef struct 
{
	BYTE	szCmd[2];
	BYTE	byResult;
} RECV_MARKING_T, *PRECV_MARKING_T;

#pragma pack()


