// mybmp.cpp : bitmap utility
//

#include "stdafx.h"
#include "MyBmp.h"


#pragma warning(disable : 4996)


CMyBitmap::CMyBitmap(void)
{
#if 0
	sprintf(m_szFrontRawFile, "%s", "d:\\temp\\front_raw.bmp");
	sprintf(m_szRearRawFile, "%s", "d:\\temp\\rear_raw.bmp");

	sprintf(m_szFrontCropFile, "%s", "d:\\temp\\front_crop.bmp");
	sprintf(m_szRearCropFile, "%s", "d:\\temp\\rear_crop.bmp");

	sprintf(m_szRawFile, "%s", "d:\\temp\\raw.bmp");
	sprintf(m_szCropFile, "%s", "d:\\temp\\crop.bmp");

#else
	sprintf(m_szFrontRawFile, "%s", "c:\\temp\\front_raw.bmp");
	sprintf(m_szRearRawFile, "%s", "c:\\temp\\rear_raw.bmp");

	sprintf(m_szFrontCropFile, "%s", "c:\\temp\\front_crop.bmp");
	sprintf(m_szRearCropFile, "%s", "c:\\temp\\rear_crop.bmp");

	sprintf(m_szRawFile, "%s", "c:\\temp\\raw.bmp");
	sprintf(m_szCropFile, "%s", "c:\\temp\\crop.bmp");
#endif

	m_pImageBuf = NULL;
}

CMyBitmap::~CMyBitmap(void)
{
	if(m_pImageBuf != NULL)
	{
		delete[] m_pImageBuf;
		m_pImageBuf = NULL;
	}
}

/***************************************************************************//**
 * @brief			Set debug mode
 * @param [in]		None
 * @retval			None
 ******************************************************************************/
void CMyBitmap::SetDebugMode(BOOL IsTrue)
{
	m_bDebug = IsTrue;
}

/***************************************************************************//**
 * @brief			Get Instance
 * @param [in]		None
 * @retval			CMyBitmap* instance
 ******************************************************************************/
CMyBitmap* CMyBitmap::GetInstance(void)
{
	static CMyBitmap *pObject = NULL;

	if(pObject == NULL)
	{
		pObject = new CMyBitmap();
	}

	return pObject;
}

/***************************************************************************//**
 * @brief			전체 파일 경로 설정
 * @param [in]		char *pFileName : 파일 경로 버퍼
 * @retval			None
 ******************************************************************************/
void CMyBitmap::SetFilePath(char *pFileName)
{
	int nLen;

	ZeroMemory(&m_szRawFile, MAX_MY_BUFFER);
	
	nLen = strlen(pFileName);
	if(nLen >= MAX_MY_BUFFER)
	{
		::CopyMemory(m_szRawFile, pFileName, nLen - 1);
	}
	else
	{
		::CopyMemory(m_szRawFile, pFileName, nLen);
	}
}

/***************************************************************************//**
 * @brief			Bitmap 파일 저장
 * @param [in]		int xsize : 이미지 width
 * @param [in]		int ysize :	이미지 height
 * @param [in]		BYTE *pImg : 이미지 data
 * @param [in]		int nBytePerPixel :	mono or color infos
 * @retval			항상 0
 ******************************************************************************/
int CMyBitmap::SaveRawImageFile(char *pFileName, BYTE *pRawData, int nDataLen)
{
	DWORD				dwWritten;
	HANDLE				hFile;

	hFile = ::CreateFile(pFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		::WriteFile(hFile, pRawData, nDataLen, &dwWritten, NULL);   
		::CloseHandle(hFile);
		return 0;
	}

	return -1;
}

/***************************************************************************//**
 * @brief			Bitmap 파일 저장
 * @param [in]		int xsize : 이미지 width
 * @param [in]		int ysize :	이미지 height
 * @param [in]		BYTE *pImg : 이미지 data
 * @param [in]		int nBytePerPixel :	mono or color infos
 * @retval			항상 0
 ******************************************************************************/
int CMyBitmap::WriteImgFile(int nDir, int xsize, int ysize, BYTE *pImg, int nBytePerPixel)
{
	BITMAPFILEHEADER	fh; 
	BITMAPINFO			bi;
	RGBQUAD				rgb[256];
	int					i, j, bfOffBits, rwsize, nSize;
	DWORD				dwWritten;
	HANDLE				hFile;

	bfOffBits = (nBytePerPixel == 1) ? 1078 : 54;

	::ZeroMemory(&fh, sizeof(BITMAPFILEHEADER));
	::ZeroMemory(&bi, sizeof(BITMAPINFO));
	::ZeroMemory(&rgb, sizeof(RGBQUAD) * 256);

	///< file header info
	{
		fh.bfType = 0x4d42;			///< "BM"
		fh.bfOffBits = bfOffBits;
		fh.bfSize = bfOffBits + bi.bmiHeader.biSizeImage;
		fh.bfReserved1 = 0;
		fh.bfReserved2 = 0;
	}

	///< bitmap header info
	{
		bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bi.bmiHeader.biWidth = xsize;
		bi.bmiHeader.biHeight = ysize;
		bi.bmiHeader.biPlanes = 1;

		bi.bmiHeader.biBitCount = 8 * nBytePerPixel;
		bi.bmiHeader.biSizeImage = xsize * ysize;
		bi.bmiHeader.biCompression = BI_RGB;
		bi.bmiHeader.biXPelsPerMeter = 0;
		bi.bmiHeader.biYPelsPerMeter = 0;
		bi.bmiHeader.biClrUsed = 0;
		bi.bmiHeader.biClrImportant = 0;
	}

	///< color table
	{
		if(nBytePerPixel == 1)
		{
			// rgb
			for( i = 0; i < 256; i++ )
			{
				rgb[i].rgbBlue = i;
				rgb[i].rgbGreen = i;
				rgb[i].rgbRed = i;
				rgb[i].rgbReserved = 0;
			}
		}
		else
		{
			for( i = 0; i < 2; i++ )
			{
				rgb[i].rgbBlue = 0xFF;
				rgb[i].rgbGreen = 0xFF;
				rgb[i].rgbRed = 0xFF;
				rgb[i].rgbReserved = 0;
			}
		}
	}

	// file save
	{
		if(nDir == TOP_DIR)
		{
			hFile = ::CreateFile(m_szFrontRawFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		}
		else
		{
			hFile = ::CreateFile(m_szRearRawFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		}
		//hFile = ::CreateFile(m_szRawFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if(hFile != INVALID_HANDLE_VALUE)
		{
			///< write bitmap file header info
			::WriteFile(hFile, &fh, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
			///< write bitmap header info
			::WriteFile(hFile, &bi, sizeof(BITMAPINFO), &dwWritten, NULL);
			///< write color table
			if(nBytePerPixel == 1)
			{
				::WriteFile(hFile, &rgb, sizeof(RGBQUAD) * 256, &dwWritten, NULL);
			}
			else
			{
				::WriteFile(hFile, &rgb, sizeof(RGBQUAD) * 1, &dwWritten, NULL);
			}

			rwsize = WIDTHBYTES(bi.bmiHeader.biBitCount, bi.bmiHeader.biWidth);

			///< write pixel data
			nSize = (ysize * rwsize) + 100;
			m_pszImgData = new BYTE[nSize];

			::ZeroMemory(m_pszImgData, nSize);
			for(i = 0; i < ysize; i++)
			{
				for(j = 0; j < rwsize; j++)
				{
					if(j >= xsize)
					{
						m_pszImgData[i * rwsize + j] = 0;
					}
					else
					{
						m_pszImgData[i * rwsize + j] = pImg[(ysize - i - 1) * xsize + j - 1];
						//m_pszImgData[i * rwsize + j] = pImg[i * rwsize + j];
					}
				}
			}
			::WriteFile(hFile, m_pszImgData, rwsize * ysize, &dwWritten, NULL);   
			::CloseHandle(hFile);

			delete[] m_pszImgData;
		}
	}

	return 0;
}

/***************************************************************************//**
 * @brief			Bitmap 파일 저장
 * @param [in]		int xsize : 이미지 width
 * @param [in]		int ysize :	이미지 height
 * @param [in]		BYTE *pImg : 이미지 data
 * @param [in]		int nBytePerPixel :	mono or color infos
 * @retval			항상 0
 ******************************************************************************/
int CMyBitmap::ReadImgFile(char *pFileName)
{
	int		nRet;
	BOOL	bRet;
	HANDLE	hFile;
	DWORD	dwSize, dwRead;

	hFile = ::CreateFile(pFileName, GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		return -1;
	}

	dwSize = ::GetFileSize(hFile, NULL);
	::SetFilePointer(hFile, (LONG)0, NULL, FILE_BEGIN);

	if(m_pImageBuf != NULL)
	{
		delete[] m_pImageBuf;
		m_pImageBuf = NULL;
	}

	m_pImageBuf = new BYTE[dwSize + 1];

	::ZeroMemory(m_pImageBuf, dwSize + 1);
	
	bRet = ::ReadFile(hFile, m_pImageBuf, dwSize, &dwRead, NULL);
	if(bRet == FALSE)
	{
		nRet = -1;
	}
	else
	{
		if(dwRead == 0)
		{
			nRet = -2;
		}
		else
		{
			nRet = (int)dwRead;
		}
	}
	::CloseHandle(hFile);

	return nRet;
}

//BITMAPFILEHEADER

void CMyBitmap::postProc(void)
{
	if (m_tBetSlip.type == 'A') 
	{
		int	i, x1,x2,y1,y2, len, cy, cx;//, bx;
		rect_t ra[12 * 6];

		len = m_tBetSlip.nRect;
		memset(ra, 0, sizeof(ra));
		if(m_tBetSlip.dir == 2)
		{
			len--;
			for (i = 0; i < m_tBetSlip.nRect; i++) 
			{
				x1 = m_tBetSlip.ra[i].top.x;
				x2 = m_tBetSlip.ra[i].bottom.x;
				y1 = m_tBetSlip.ra[i].top.y;
				y2 = m_tBetSlip.ra[i].bottom.y;

				ra[len-i].top.x = m_tBetSlip.width - x2;
				ra[len - i].top.y = m_tBetSlip.height - y2;
				ra[len - i].bottom.x = m_tBetSlip.width - x1;
				ra[len - i].bottom.y = m_tBetSlip.height - y1;
			}
		}
		else 
		{
			memcpy(ra, m_tBetSlip.ra, sizeof(rect_t)*len);
		}

		if(m_tBetSlip.nRect < 12 && ra[0].top.y != ra[11].top.y) 
		{
			printf("barcode 갯수 인식오류\n"); 
			return;
		}

		y1 = ra[0].bottom.y - ra[0].top.y;
		cy = ra[12].top.y - ra[0].top.y;
		if (cy > y1 * 2) cy = y1 * 3 / 2;

		cx = (int)(ra[11].bottom.x - ra[0].top.x + (PIX_IN_MM * 1.5));
		cx /= 12;

		for (i = 0; i < 12; i++) 
		{
			m_tBetSlip.rbin[i] = 0x23;
		}

		for (; i < m_tBetSlip.nRect; i++) 
		{
			// conv rect data  to  binary
			// check x,y position
			x1 = (ra[i].bottom.x - ra[0].top.x-cx/2) / cx;
			y1 = (ra[i].bottom.y - ra[0].top.y) / cy;
			m_tBetSlip.rbin[x1] |= (1 << (5-y1));
		}

		//for (i = 0; i < 12; i++)
		//	printf("%02x ", m_tBetSlip.rbin[i]);
		//printf("\n");
	}
}

void CMyBitmap::calcSumImg(unsigned int *sum,int x1, int y1, int x2, int y2, char mode)
{
	unsigned char *p;
	unsigned char *img = m_tBetSlip.img;
	int col, row;

	// origin position
	if (mode == 0) 
	{
		p = img + y1 * m_tBetSlip.byte_per_row;
		for (row = y1;  row < y2; row++, p += m_tBetSlip.byte_per_row) 
		{
			for (col = x1; col < x2; col++) 
			{
				sum[row] += ((*(p + col) >= THRESHHOLD) ? 0 : 1);
			}
		}
	}
	else
	{
		for (col = x1; col < x2; col++) 
		{
			p = img + y1 * m_tBetSlip.byte_per_row + col;
			for (row = y1; row < y2; row++, p += m_tBetSlip.byte_per_row) 
			{
				sum[col] += ((*p >= THRESHHOLD) ? 0 : 1);
			}
		}
	}
}

void CMyBitmap::find_1_image(unsigned int *sum_a, int st, int ed, unsigned short *seg, short *lenseg)
{
	int	flag, index, x0;
	short i, len;

	len = 0;
	flag = sum_a[st];
	if (flag) 
	{
		x0 = st;
		index = 1;
	}
	else 
	{
		index = 0;
		x0 = 0;
	}

	for (i = st; i < ed; i++) 
	{
		if (sum_a[i] && flag) 
		{
			continue;
		}

		if (!(sum_a[i] || flag)) 
		{
			continue;
		}
		
		flag = sum_a[i];
		index++;
		if (index & 1) 
		{
			x0 = i;
		}
		else 
		{
			seg[len++] = x0;
			seg[len++] = i;
		}
	}

	if (flag && (index&1)) 
	{
		seg[len++] = x0;
		seg[len++] = i;
	}
	*lenseg = len;
}

int CMyBitmap::find_rect(void)
{
	//unsigned char *p;
	unsigned char *img = m_tBetSlip.img;
	unsigned short seg_y[200], y_st, y_ed;
	unsigned short seg_x[200], x_st, x_ed;
	short i, j, lenseg_y, lenseg_x, pd, nRect;

	memset(seg_y, 0, sizeof(seg_y));
	nRect = 0;
	
	calcSumImg((unsigned int *)sum_xa, 10, 0, m_tBetSlip.width-10, m_tBetSlip.height, 0);
	//calcSumImg((unsigned int *)sum_xa, 0, 0, m_tBetSlip.width, m_tBetSlip.height, 0);
	find_1_image((unsigned int *)sum_xa, 0, m_tBetSlip.height, seg_y, &lenseg_y);

	for (i = 0; i < lenseg_y; i += 2) 
	{
		y_st = seg_y[i];
		y_ed = seg_y[i + 1];

		pd = y_ed - y_st;
		if (pd < RECT_Y_MIN) continue;
		if (pd > RECT_Y_MAX) continue;

		memset(sum_ya, 0, sizeof(sum_ya));
		calcSumImg((unsigned int *)sum_ya, 0, y_st, m_tBetSlip.width, y_ed, 1);
		memset(seg_x, 0, sizeof(seg_x));
		lenseg_x = 0;
		find_1_image((unsigned int *)sum_ya, 0, m_tBetSlip.width, seg_x, &lenseg_x);
		for (j = 0; j < lenseg_x; j += 2) 
		{
			x_st = seg_x[j];
			x_ed = seg_x[j + 1];

			pd = x_ed - x_st;
			if (pd < RECT_X_MIN) 
			{
				continue;
			}
			if (pd > RECT_X_MAX) 
			{
				continue;
			}
			
			m_tBetSlip.ra[nRect].top.x = x_st;
			m_tBetSlip.ra[nRect].top.y = y_st;
			m_tBetSlip.ra[nRect].bottom.x = x_ed;
			m_tBetSlip.ra[nRect].bottom.y = y_ed;
			nRect++;
		}
	}

	m_tBetSlip.nRect = nRect;

	return nRect;
}

void CMyBitmap::checkOrigin(void)
{
	int	org = 0;
	int	ref_x, ref_y;
	int i,d, nx0, nx1, ny0, ny1;

	d = 0;
	nx0 = nx1 = ny0 = ny1 = 0;
	if (m_tBetSlip.type == 'A') 
	{	///< 구매권
		ref_y = PIX_IN_MM * 25;
		for (i = 0; i < m_tBetSlip.nRect; i++) 
		{
			if (m_tBetSlip.ra[i].top.y < ref_y) 
			{
				ny0++;
				if (i != d) 
				{
					memcpy(&m_tBetSlip.ra[d++], &m_tBetSlip.ra[i], sizeof(rect_t));
				}
				else 
				{
					d++;
				}
			}
			else if (m_tBetSlip.ra[i].bottom.y > (m_tBetSlip.height - ref_y)) 
			{
				ny1++;
				if (i != d) 
				{
					memcpy(&m_tBetSlip.ra[d++], &m_tBetSlip.ra[i], sizeof(rect_t));
				}
				else 
				{
					d++;
				}
			}
		}
		m_tBetSlip.nRect = d;

		if (ny1 > 1) org |= 8;	//y_bottom
		if (ny0 > 1) org |= 4;	//y_top
		//if (nx1 > 30) org |= 2; //x_right
		//if (nx0 > 30) org |= 1;	//x_left

		if (org == 4) 
			m_tBetSlip.dir = 1;		//x_right, y_top
		else if (org == 8) 
			m_tBetSlip.dir = 2;	//x_left, y_bottom
		//else if (org == 5) m_tBetSlip.dir = 3;	//x_left, y_top
		//else if (org == 0xa) m_tBetSlip.dir = 4;//x_right, y_bottom

	} 
	else 
	{	///< 구매표
		ref_x = PIX_IN_MM * 6;
		ref_y = PIX_IN_MM * 16;

		LogOut("[checkOrigin] nRect = %d, ref_x = %d, ref_y = %d \n", m_tBetSlip.nRect, ref_x, ref_y);

		for (i = 0; i < m_tBetSlip.nRect; i++) 
		{
			//LogOut("[checkOrigin] i(%02d), x1(%d), y1(%d), x2(%d), y2(%d) \n", 
			//		i, m_tBetSlip.ra[i].top.x, m_tBetSlip.ra[i].top.y,
			//		m_tBetSlip.ra[i].bottom.x, m_tBetSlip.ra[i].bottom.y);

			if (m_tBetSlip.ra[i].top.x < ref_x) 
			{
				nx0++;
			}
			else if (m_tBetSlip.ra[i].bottom.x > (m_tBetSlip.width - ref_x))
			{
				nx1++; 
			}
			else if (m_tBetSlip.ra[i].top.y < ref_y) 
			{
				ny0++; 
			}
			else if (m_tBetSlip.ra[i].bottom.y > (m_tBetSlip.height - ref_y)) 
			{
				ny1++;
			}
			else 
			{
				memcpy(&m_tBetSlip.ra[d++], &m_tBetSlip.ra[i], sizeof(rect_t));
			}
		}

		m_tBetSlip.nRect = d;

		//LogOut("[checkOrigin] nx0 = %d, nx1 = %d, ny0 = %d, ny1 = %d \n", nx0, nx1, ny0, ny1);

		if (ny1 > 1) org |= 8;	//y_bottom
		if (ny0 > 1) org |= 4;	//y_top
		if (nx1 > 20) org |= 2; //x_right
		if (nx0 > 20) org |= 1;	//x_left

		//LogOut("[checkOrigin] org = %d \n", org);

		switch(org)
		{
		case 12 :
		case 4 :
			m_tBetSlip.dir = 1;		//x_right, y_top
			break;
		case 6 :
			m_tBetSlip.dir = 3;	//x_left, y_top
			break;
		case 8 :
		case 9 :
			m_tBetSlip.dir = 2;	//x_left, y_bottom
			break;
		case 10 :
			m_tBetSlip.dir = 4;//x_right, y_bottom
			break;
		}

		LogOut("[checkOrigin] org = %d, dir = %d \n", org, m_tBetSlip.dir);

		/***
		if (org == 6) 
			m_tBetSlip.dir = 1;		//x_right, y_top
		else if (org == 9) 
			m_tBetSlip.dir = 2;	//x_left, y_bottom
		else if (org == 5) 
			m_tBetSlip.dir = 3;	//x_left, y_top
		else if (org == 0xa) 
			m_tBetSlip.dir = 4;//x_right, y_bottom
		***/
	}
	return;
}

int CMyBitmap::checkSlip(void)
{
	LogOut("[%s] m_tBetSlip.height(%d), m_tBetSlip.width(%d) \n", __FUNCTION__, m_tBetSlip.height, m_tBetSlip.width);

	//1. 세로길이 > 가로길이 * 1.5 : 구매표('B') else 구매권('A')
	if ((m_tBetSlip.height > m_tBetSlip.width*1.5) && (m_tBetSlip.width > PIX_IN_MM * 80)) 
	{
		m_tBetSlip.type = 'B';	//구매표
	}
	else if ((m_tBetSlip.height < m_tBetSlip.width*1.5) && (m_tBetSlip.width > PIX_IN_MM * 80)) 
	{
		m_tBetSlip.type = 'A';	//구매권
	}
	else
	{
		m_tBetSlip.type = 'C';	//오류
	}

	return 0;
}

char s_Buffer[1024 * 5];

int CMyBitmap::ImageProcessing(int nDir, BYTE *retBuf)
{
	int					i;
	int					nRet, nOffset;
	int					nByte;
	BITMAPFILEHEADER	fh; 
	BITMAPINFO			bi;

	LogOut("ImageProcessing Start!\n");

	if(nDir == TOP_DIR)
	{
		nRet = ReadImgFile(m_szFrontCropFile);
	}
	else
	{
		nRet = ReadImgFile(m_szRearCropFile);
	}
	//nRet = ReadFile(m_szCropFile);
	if(nRet < 0)
	{
		return nRet;
	}

	nOffset = 0;

	if(m_pImageBuf == NULL)
	{
		return -1;
	}

	///< bitmap_file_header
	::CopyMemory(&fh, &m_pImageBuf[nOffset], sizeof(BITMAPFILEHEADER));
	nOffset += sizeof(BITMAPFILEHEADER);

	///< bitmap_info
	::CopyMemory(&bi, &m_pImageBuf[nOffset], sizeof(BITMAPINFO));
	nOffset += sizeof(BITMAPINFO);

	if(bi.bmiHeader.biCompression != 0)
	{
		;
	}
	
	{
		LogOut("%s", "[BITMAP_FILE_INFO]\n");

		LogOut("sizeof(BITMAPFILEHEADER) = %d\nsizeof(BITMAPINFO) = %d\nsizeof(BITMAPINFOHEADER) = %d\nsizeof(RGBQUAD) = %d\n", sizeof(BITMAPFILEHEADER), sizeof(BITMAPINFO), sizeof(BITMAPINFOHEADER), sizeof(RGBQUAD));

		LogOut("[BITMAP_FILE_HEADER] - bfType = %04X \n", fh.bfType);
		LogOut("[BITMAP_FILE_HEADER] - bfSize = %d \n", fh.bfSize);
		LogOut("[BITMAP_FILE_HEADER] - bfOffBits = %d \n\n", fh.bfOffBits);

		LogOut("[BITMAP_INFO_HEADER] - biSize = %d \n", bi.bmiHeader.biSize);
		LogOut("[BITMAP_INFO_HEADER] - biWidth = %d \n", bi.bmiHeader.biWidth);
		LogOut("[BITMAP_INFO_HEADER] - biHeight = %d \n", bi.bmiHeader.biHeight);
		LogOut("[BITMAP_INFO_HEADER] - biPlanes = %d \n", bi.bmiHeader.biPlanes);
		LogOut("[BITMAP_INFO_HEADER] - biBitCount = %d \n", bi.bmiHeader.biBitCount);
		LogOut("[BITMAP_INFO_HEADER] - biCompression = %d \n", bi.bmiHeader.biCompression);
		LogOut("[BITMAP_INFO_HEADER] - biSizeImage = %d \n", bi.bmiHeader.biSizeImage);
		LogOut("[BITMAP_INFO_HEADER] - biXPelsPerMeter = %d \n", bi.bmiHeader.biXPelsPerMeter);
		LogOut("[BITMAP_INFO_HEADER] - biYPelsPerMeter = %d \n", bi.bmiHeader.biYPelsPerMeter);
		LogOut("[BITMAP_INFO_HEADER] - biClrUsed = %d \n", bi.bmiHeader.biClrUsed);
		LogOut("[BITMAP_INFO_HEADER] - biClrImportant = %d \n", bi.bmiHeader.biClrImportant);

		LogOut("[RGBQUAD] - Blue/Green/Red = %d, %d, %d \n", bi.bmiColors[0].rgbBlue, bi.bmiColors[0].rgbGreen, bi.bmiColors[0].rgbRed);
		LogOut("%s", "\n");
	}

	m_nFrameWidth = bi.bmiHeader.biWidth;
	m_nFrameHeight = (bi.bmiHeader.biHeight > 0) ? bi.bmiHeader.biHeight : -bi.bmiHeader.biHeight;	// abs, Dimensions in pixels

	if(bi.bmiHeader.biBitCount == 0)
	{
		bi.bmiHeader.biBitCount = 1;
	}
	
	m_nBytesPerRow = m_nFrameWidth * bi.bmiHeader.biBitCount / 8;	// Width (in bytes) including alignment!
	if(m_nBytesPerRow & 3) 
	{
		m_nBytesPerRow += 4;
		m_nBytesPerRow /= 4;
		m_nBytesPerRow *= 4;
	}

	nByte = bi.bmiHeader.biBitCount / 8;

	// *4* LOAD RASTER DATA
	// Seek Raster Data in file
	fcur = fh.bfOffBits;

	//=====================================
	//memset(xa, 0, sizeof(xa));
	//memset(ya, 0, sizeof(ya));
	memset(sum_xa, 0, sizeof(sum_xa));
	memset(sum_ya, 0, sizeof(sum_ya));
	memset((char *)&m_tBetSlip, 0, sizeof(m_tBetSlip));

	m_tBetSlip.height = m_nFrameHeight;
	m_tBetSlip.width = m_nFrameWidth;
	m_tBetSlip.byte_per_row = m_nBytesPerRow;
	m_tBetSlip.img = (m_pImageBuf + fcur);	//real image poition
	
	///< ImageProcessing

	LogOut("Processing Start!\n");
	
	checkSlip();	//구매표 ? 구매권
	printf("CheckSlip End!\n");
	
	if (m_tBetSlip.type == 'C') 
	{
		LogOut("%s", "[ERROR] type C End!\n");
		return -1;
	}
	
	nRet = find_rect();
	LogOut("find_rect(), nRet(%d) \n", nRet);
	if (nRet < 12) 
	{
		LogOut("%s", "[ERROR] findrect < 12 End!\n");
		return -2;
	}
	checkOrigin();
	postProc();

	LogOut("[CMyBitmap::ImageProcessing] m_tBetSlip.type(%d), m_tBetSlip.dir(%d) \n", m_tBetSlip.type, m_tBetSlip.dir);

	nOffset = 0;

	nOffset += sprintf((char *)&s_Buffer[nOffset], "%c,%d\n", m_tBetSlip.type, m_tBetSlip.dir );
	if (m_tBetSlip.type == 'A') 
	{
		for (i = 0; i < 12; i++) 
		{
			nOffset += sprintf((char *)&s_Buffer[nOffset], "%02X ", m_tBetSlip.rbin[i]);
		}
		nOffset += sprintf((char *)&s_Buffer[nOffset], "\n");
	}
	
	for (i = 0; i < m_tBetSlip.nRect; i++) 
	{
		nOffset += sprintf((char *)&s_Buffer[nOffset], "%d,%d,%d,%d,%d,%d\n", 
						   m_tBetSlip.ra[i].top.x, 
						   m_tBetSlip.ra[i].top.y, 
						   m_tBetSlip.ra[i].bottom.x, 
						   m_tBetSlip.ra[i].bottom.y, 
						   m_tBetSlip.ra[i].bottom.x- m_tBetSlip.ra[i].top.x, 
						   m_tBetSlip.ra[i].bottom.y - m_tBetSlip.ra[i].top.y);
	}
	
 	if(retBuf != NULL)
 	{
 		::CopyMemory(retBuf, s_Buffer, strlen(s_Buffer));
		LogOut("result_0 = %s \n", s_Buffer);
 	}
	else
	{
		LogOut("result_null = %s \n", s_Buffer);
	}

	return 0;
}

/*
static char usrMsg[1024*8];
SYSTEMTIME lt;

int CMyBitmap::LogOut(const char *fmt, ...)
{
	FILE	*fd;
	va_list ap;
	int ltime;
	char name[64], path[256];
	
	if (m_bDebug == true)
	{
		va_start(ap, fmt);
		vsprintf(usrMsg, fmt, ap);
		va_end(ap);

		memset(name, 0, sizeof(name));
		memset(path, 0, sizeof(path));

		GetLocalTime(&lt);
		strcpy(path, "c:\\temp\\");
		sprintf(name, "OMR_%04d%02d%02d.log", lt.wYear, lt.wMonth, lt.wDay);
		strcat(path, name);

		if ((fd = fopen(path, "a+")) == NULL)
		{
			printf("can't open %s\n", path);
			return -1;
		}

		ltime = lt.wHour * 10000 + lt.wMinute * 100 + lt.wSecond;
		fprintf(fd, "[%02d:%02d:%02d] %s", lt.wHour, lt.wMinute, lt.wSecond, usrMsg);
		fclose(fd);
	}

	return 0;
}
*/

/*
int CMyBitmap::trace(const char *fmt, ...)
{
	FILE	*fd;
	va_list ap;
	char name[64], path[256];
	
	va_start(ap, fmt);
	vsprintf(usrMsg, fmt, ap);
	va_end(ap);

	memset(name,0,sizeof(name));
	memset(path, 0, sizeof(path));
		
	GetLocalTime(&lt);

	strcpy(path, "c:\\temp\\");
	sprintf(name,"OMR_%04d%02d%02d.log", lt.wYear, lt.wMonth, lt.wDay);
	strcat(path, name);

	if ((fd = fopen(path, "a+")) == NULL)
	{
		printf("can't open %s\n", path);
		return -1;
	}
	fprintf(fd,"%s", usrMsg);
	fclose(fd);

	return 0;
}
*/

/*
void CMyBitmap::HexaDump(char *pTitle, BYTE *data, int nLen)
{
	int i, off;
	char szTemp[256], *cp=szTemp;
	SYSTEMTIME lt;

	if (m_bDebug == true)
	{
		off = 0;

		GetLocalTime(&lt);

		sprintf(szTemp, "[%02d:%02d:%02d] (%s) HexDump : Len = [%d] ", lt.wHour, lt.wMinute, lt.wSecond, pTitle, nLen);
		trace("%s", szTemp);

		for (i = 0; i < nLen; i++)
		{
			if (((i % 16) == 0) && i)
			{
				//cp += sprintf(cp, "\n");
				trace(">%s", szTemp);
				cp = szTemp;
			}
			if ((i % 4) == 0)
			{
				cp += sprintf(cp, " ");
			}
			cp += sprintf(cp, "[%02X]", data[i] & 0xFF);
		}
		trace(">%s", szTemp);

		sprintf(szTemp, "--------------------------------------------------------------------\n");
		trace(">%s", szTemp);
	}
}*/

