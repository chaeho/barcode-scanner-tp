// DevUsb.h : DevUsb.cpp의 기본 헤더 파일입니다.
//

#pragma once

#include "lusb0_usb.h"


// Device vendor and product id.
#define MY_VID				0x0D03
#define	MY_PID				0x0102

// Device configuration and interface id.
#define MY_CONFIG			1
#define MY_INTF				0

// Device endpoint(s)
#define EP_IN				0x81
#define EP_OUT				0x01

// Device of bytes to transfer.
#define BUF_SIZE			1728

#define MAX_USB_TMO			1000

// Fiedl length definition
#define DEVICE_SN_LENGTH	(32)


#pragma pack(1)

typedef struct
{
	BYTE	szCmd[2];		///> 명령
	BYTE	byResult;		///> 스캔 결과
	BYTE	byState;		///> 스캔상태
	BYTE	byError;		///> 에러상태
	BYTE	byTicket;		///> 마권정보 ('A':구매권, 'B':구매표, 'C':알수없음)
	BYTE	szScanLine[2];	///> Scan Line
	BYTE	byScanSide;		///> 스캔 Side (0x00:bottom, 0x01:top)
	BYTE	byScanDir;		///> 스캔 방향 (0x46:정방향, 0x52:역방향)
	WORD	wPos[58];		///> 구매표(32), 구매권(6)

} RECOG_RESULT_T, *PRECOG_RESULT_T;


#pragma pack()

////////////////////////////////////////////////////

class CDevLibUsb
{
public:
	CDevLibUsb(void);
	virtual ~CDevLibUsb(void);

	static CDevLibUsb* GetInstance(void);

public:
	usb_dev_handle*		m_phUsb;
	BOOL				m_bIsOpen;
	BOOL				m_bIsDebug;

public:
	void SetDebugMode(BOOL bIsDebug);

	int BigToWord(BYTE *pbSrcBig);
	int WordToBig(int wSrc, BYTE *pbDestBig);
	int OpenDevice(void);
	int CloseDevice(void);

	int Reset(void);
	int Get_Fimware_Version(BYTE *data);
	int Get_Serial_Num(BYTE *data);
	int Set_Serial_Num(BYTE *Send_Data, int Len);
	int Check_Status(BYTE *data);
	int Get_Param(BYTE *data);
	int Set_Param(BYTE *Send_Data, int Len);
	int Motor_On(BYTE identifier, BYTE dir);
	int Motor_Off(BYTE identifier);
	int Led_Control(BYTE is_on);
	int Scan_Start(unsigned int IsOMR);
	int Scan_Stop(void);
	int Get_Image(int Line, unsigned char *FrontRawBuf, unsigned char *RearRawBuf);
	int Get_Axis(unsigned char *data);
	int Eject(BYTE Type);
	int Reject(void);
	int TPH_BrandInfo(unsigned short Len);
    int TPH_BrandData(unsigned char *pImage, int Len);
	int TPH_Branding(BYTE dir, unsigned short PositionY);

	int Calibration(unsigned char step);
	int Firmware_Update(BYTE* Send_Data, int Len);
	int Factory_Init(void);

	int LogOut(const char *fmt, ...);
	int trace(const char *fmt, ...);
	void HexaDump(char *pTitle, BYTE *data, int nLen);
};





